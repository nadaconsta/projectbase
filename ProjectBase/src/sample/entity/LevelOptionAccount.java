package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_OPTION_ACCOUNT database table.
 * 
 */
@Entity
@Table(name = "LEVEL_OPTION_ACCOUNT")
@NamedQuery(name = "LevelOptionAccount.findAll", query = "SELECT l FROM LevelOptionAccount l")
public class LevelOptionAccount extends GenericEntity<LevelOptionAccount>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_OPTION_ACCOUNT", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelOptionAccount;

	@Column(name = "DESC", nullable = false, length = 255)
	private String value;

	// bi-directional many-to-one association to Level
	@OneToMany(mappedBy = "levelOptionAccount")
	private List<Level> levels;

	// bi-directional many-to-one association to LevelTypeAccountGame
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_TYPE_ACCOUNT_GAME", nullable = false)
	private LevelTypeAccountGame levelTypeAccountGame;

	public LevelOptionAccount() {
	}

	public int getIdLevelOptionAccount() {
		return this.idLevelOptionAccount;
	}

	public void setIdLevelOptionAccount(int idLevelOptionAccount) {
		this.idLevelOptionAccount = idLevelOptionAccount;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<Level> getLevels() {
		return this.levels;
	}

	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public Level addLevel(Level level) {
		getLevels().add(level);
		level.setLevelOptionAccount(this);

		return level;
	}

	public Level removeLevel(Level level) {
		getLevels().remove(level);
		level.setLevelOptionAccount(null);

		return level;
	}

	public LevelTypeAccountGame getLevelTypeAccountGame() {
		return this.levelTypeAccountGame;
	}

	public void setLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		this.levelTypeAccountGame = levelTypeAccountGame;
	}

}