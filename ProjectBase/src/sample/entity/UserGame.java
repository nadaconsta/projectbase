package sample.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the USER_GAME database table.
 * 
 */
@Entity
@Table(name = "USER_GAME")
@NamedQuery(name = "UserGame.findAll", query = "SELECT u FROM UserGame u")
public class UserGame extends GenericEntity<UserGame> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER_GAME", unique = true, nullable = false)
	private int idUserGame;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_SINCE")
	private Date dtSince;

	// bi-directional many-to-one association to AccountUserGame
	@OneToMany(mappedBy = "userGame")
	private List<AccountUserGame> accountUserGames;

	// bi-directional many-to-one association to CharacterUserGame
	@OneToMany(mappedBy = "userGame")
	private List<CharacterUserGame> characterUserGames;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to Proficiency
	@ManyToOne
	@JoinColumn(name = "ID_PROFICIENCY", nullable = false)
	private Proficiency proficiency;

	// bi-directional many-to-one association to Schedule
	@OneToOne
	@JoinColumn(name = "ID_SCHEDULE", nullable = false)
	private Schedule schedule;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "ID_USER", nullable = false)
	private User user;

	public UserGame() {
	}

	public int getIdUserGame() {
		return this.idUserGame;
	}

	public void setIdUserGame(int idUserGame) {
		this.idUserGame = idUserGame;
	}

	public Date getDtSince() {
		return this.dtSince;
	}

	public void setDtSince(Date dtSince) {
		this.dtSince = dtSince;
	}

	public List<AccountUserGame> getAccountUserGames() {
		return this.accountUserGames;
	}

	public void setAccountUserGames(List<AccountUserGame> accountUserGames) {
		this.accountUserGames = accountUserGames;
	}

	public AccountUserGame addAccountUserGame(AccountUserGame accountUserGame) {
		getAccountUserGames().add(accountUserGame);
		accountUserGame.setUserGame(this);

		return accountUserGame;
	}

	public AccountUserGame removeAccountUserGame(AccountUserGame accountUserGame) {
		getAccountUserGames().remove(accountUserGame);
		accountUserGame.setUserGame(null);

		return accountUserGame;
	}

	public List<CharacterUserGame> getCharacterUserGames() {
		return this.characterUserGames;
	}

	public void setCharacterUserGames(List<CharacterUserGame> characterUserGames) {
		this.characterUserGames = characterUserGames;
	}

	public CharacterUserGame addCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().add(characterUserGame);
		characterUserGame.setUserGame(this);

		return characterUserGame;
	}

	public CharacterUserGame removeCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().remove(characterUserGame);
		characterUserGame.setUserGame(null);

		return characterUserGame;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Proficiency getProficiency() {
		return this.proficiency;
	}

	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}