package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GAME database table.
 * 
 */
@Entity
@Table(name = "GAME")
@NamedQuery(name = "Game.findAll", query = "SELECT g FROM Game g")
public class Game extends GenericEntity<Game> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGame;

	@Column(nullable = false, length = 5)
	private String acronym;

	@Column(nullable = false, length = 255)
	private String name;

	@Lob
	@Column(nullable = false)
	private byte[] pic;

	// bi-directional many-to-one association to Character
	@OneToMany(mappedBy = "game")
	private List<Character> characters;

	// bi-directional many-to-one association to Developer
	@ManyToOne
	@JoinColumn(name = "ID_DEVELOPER", nullable = false)
	private Developer developer;

	// bi-directional many-to-one association to GameType
	@ManyToOne
	@JoinColumn(name = "ID_GAME_TYPE", nullable = false)
	private GameType gameType;

	// bi-directional many-to-one association to Group
	@OneToMany(mappedBy = "game")
	private List<Group> groups;

	// bi-directional many-to-one association to GroupPattern
	@OneToMany(mappedBy = "game")
	private List<GroupPattern> groupPatterns;

	// bi-directional many-to-one association to LevelTypeAccountGame
	@OneToMany(mappedBy = "game")
	private List<LevelTypeAccountGame> levelTypeAccountGames;

	// bi-directional many-to-one association to LevelTypeCharacterGame
	@OneToMany(mappedBy = "game")
	private List<LevelTypeCharacterGame> levelTypeCharacterGames;

	// bi-directional many-to-one association to Role
	@OneToMany(mappedBy = "game")
	private List<Role> roles;

	// bi-directional many-to-one association to Server
	@OneToMany(mappedBy = "game")
	private List<Server> servers;

	// bi-directional many-to-one association to UserGame
	@OneToMany(mappedBy = "game")
	private List<UserGame> userGames;

	public Game() {
	}

	public int getIdGame() {
		return this.idGame;
	}

	public void setIdGame(int idGame) {
		this.idGame = idGame;
	}

	public String getAcronym() {
		return this.acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getPic() {
		return this.pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public List<Character> getCharacters() {
		return this.characters;
	}

	public void setCharacters(List<Character> characters) {
		this.characters = characters;
	}

	public Character addCharacter(Character character) {
		getCharacters().add(character);
		character.setGame(this);

		return character;
	}

	public Character removeCharacter(Character character) {
		getCharacters().remove(character);
		character.setGame(null);

		return character;
	}

	public Developer getDeveloper() {
		return this.developer;
	}

	public void setDeveloper(Developer developer) {
		this.developer = developer;
	}

	public GameType getGameType() {
		return this.gameType;
	}

	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Group addGroup(Group group) {
		getGroups().add(group);
		group.setGame(this);

		return group;
	}

	public Group removeGroup(Group group) {
		getGroups().remove(group);
		group.setGame(null);

		return group;
	}

	public List<GroupPattern> getGroupPatterns() {
		return this.groupPatterns;
	}

	public void setGroupPatterns(List<GroupPattern> groupPatterns) {
		this.groupPatterns = groupPatterns;
	}

	public GroupPattern addGroupPattern(GroupPattern groupPattern) {
		getGroupPatterns().add(groupPattern);
		groupPattern.setGame(this);

		return groupPattern;
	}

	public GroupPattern removeGroupPattern(GroupPattern groupPattern) {
		getGroupPatterns().remove(groupPattern);
		groupPattern.setGame(null);

		return groupPattern;
	}

	public List<LevelTypeAccountGame> getLevelTypeAccountGames() {
		return this.levelTypeAccountGames;
	}

	public void setLevelTypeAccountGames(
			List<LevelTypeAccountGame> levelTypeAccountGames) {
		this.levelTypeAccountGames = levelTypeAccountGames;
	}

	public LevelTypeAccountGame addLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		getLevelTypeAccountGames().add(levelTypeAccountGame);
		levelTypeAccountGame.setGame(this);

		return levelTypeAccountGame;
	}

	public LevelTypeAccountGame removeLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		getLevelTypeAccountGames().remove(levelTypeAccountGame);
		levelTypeAccountGame.setGame(null);

		return levelTypeAccountGame;
	}

	public List<LevelTypeCharacterGame> getLevelTypeCharacterGames() {
		return this.levelTypeCharacterGames;
	}

	public void setLevelTypeCharacterGames(
			List<LevelTypeCharacterGame> levelTypeCharacterGames) {
		this.levelTypeCharacterGames = levelTypeCharacterGames;
	}

	public LevelTypeCharacterGame addLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		getLevelTypeCharacterGames().add(levelTypeCharacterGame);
		levelTypeCharacterGame.setGame(this);

		return levelTypeCharacterGame;
	}

	public LevelTypeCharacterGame removeLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		getLevelTypeCharacterGames().remove(levelTypeCharacterGame);
		levelTypeCharacterGame.setGame(null);

		return levelTypeCharacterGame;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setGame(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setGame(null);

		return role;
	}

	public List<Server> getServers() {
		return this.servers;
	}

	public void setServers(List<Server> servers) {
		this.servers = servers;
	}

	public Server addServer(Server server) {
		getServers().add(server);
		server.setGame(this);

		return server;
	}

	public Server removeServer(Server server) {
		getServers().remove(server);
		server.setGame(null);

		return server;
	}

	public List<UserGame> getUserGames() {
		return this.userGames;
	}

	public void setUserGames(List<UserGame> userGames) {
		this.userGames = userGames;
	}

	public UserGame addUserGame(UserGame userGame) {
		getUserGames().add(userGame);
		userGame.setGame(this);

		return userGame;
	}

	public UserGame removeUserGame(UserGame userGame) {
		getUserGames().remove(userGame);
		userGame.setGame(null);

		return userGame;
	}

}