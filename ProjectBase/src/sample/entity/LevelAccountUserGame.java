package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_ACCOUNT_USER_GAME database table.
 *
 */
@Entity
@Table(name = "LEVEL_ACCOUNT_USER_GAME")
@NamedQuery(name = "LevelAccountUserGame.findAll", query = "SELECT l FROM LevelAccountUserGame l")
public class LevelAccountUserGame extends GenericEntity<LevelAccountUserGame>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_ACCOUNT_USER_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelAccountUserGame;

	// bi-directional many-to-one association to AccountUserGame
	@ManyToOne
	@JoinColumn(name = "ID_ACCOUNT", nullable = false)
	private AccountUserGame accountUserGame;

	// bi-directional many-to-one association to Level
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL", nullable = false)
	private Level level;

	// bi-directional many-to-one association to LevelTypeAccountGame
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_TYPE_ACCOUNT_GAME", nullable = false)
	private LevelTypeAccountGame levelTypeAccountGame;

	public LevelAccountUserGame() {
	}

	public int getIdLevelAccountUserGame() {
		return this.idLevelAccountUserGame;
	}

	public void setIdLevelAccountUserGame(int idLevelAccountUserGame) {
		this.idLevelAccountUserGame = idLevelAccountUserGame;
	}

	public AccountUserGame getAccountUserGame() {
		return this.accountUserGame;
	}

	public void setAccountUserGame(AccountUserGame accountUserGame) {
		this.accountUserGame = accountUserGame;
	}

	public Level getLevel() {
		return this.level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public LevelTypeAccountGame getLevelTypeAccountGame() {
		return this.levelTypeAccountGame;
	}

	public void setLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		this.levelTypeAccountGame = levelTypeAccountGame;
	}

}