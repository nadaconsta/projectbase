package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_RANGE database table.
 *
 */
@Entity
@Table(name = "LEVEL_RANGE")
@NamedQuery(name = "LevelRange.findAll", query = "SELECT l FROM LevelRange l")
public class LevelRange extends GenericEntity<LevelRange> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_RANGE", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelRange;

	@Column(name = "NUM_BEGIN", nullable = false)
	private int numBegin;

	@Column(name = "NUM_END", nullable = false)
	private int numEnd;

	// bi-directional many-to-one association to LevelTypeAccountGame
	@OneToMany(mappedBy = "levelRange")
	private List<LevelTypeAccountGame> levelTypeAccountGames;

	// bi-directional many-to-one association to LevelTypeCharacterGame
	@OneToMany(mappedBy = "levelRange")
	private List<LevelTypeCharacterGame> levelTypeCharacterGames;

	public LevelRange() {
	}

	public int getIdLevelRange() {
		return this.idLevelRange;
	}

	public void setIdLevelRange(int idLevelRange) {
		this.idLevelRange = idLevelRange;
	}

	public int getNumBegin() {
		return this.numBegin;
	}

	public void setNumBegin(int numBegin) {
		this.numBegin = numBegin;
	}

	public int getNumEnd() {
		return this.numEnd;
	}

	public void setNumEnd(int numEnd) {
		this.numEnd = numEnd;
	}

	public List<LevelTypeAccountGame> getLevelTypeAccountGames() {
		return this.levelTypeAccountGames;
	}

	public void setLevelTypeAccountGames(
			List<LevelTypeAccountGame> levelTypeAccountGames) {
		this.levelTypeAccountGames = levelTypeAccountGames;
	}

	public LevelTypeAccountGame addLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		getLevelTypeAccountGames().add(levelTypeAccountGame);
		levelTypeAccountGame.setLevelRange(this);

		return levelTypeAccountGame;
	}

	public LevelTypeAccountGame removeLevelTypeAccountGame(
			LevelTypeAccountGame levelTypeAccountGame) {
		getLevelTypeAccountGames().remove(levelTypeAccountGame);
		levelTypeAccountGame.setLevelRange(null);

		return levelTypeAccountGame;
	}

	public List<LevelTypeCharacterGame> getLevelTypeCharacterGames() {
		return this.levelTypeCharacterGames;
	}

	public void setLevelTypeCharacterGames(
			List<LevelTypeCharacterGame> levelTypeCharacterGames) {
		this.levelTypeCharacterGames = levelTypeCharacterGames;
	}

	public LevelTypeCharacterGame addLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		getLevelTypeCharacterGames().add(levelTypeCharacterGame);
		levelTypeCharacterGame.setLevelRange(this);

		return levelTypeCharacterGame;
	}

	public LevelTypeCharacterGame removeLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		getLevelTypeCharacterGames().remove(levelTypeCharacterGame);
		levelTypeCharacterGame.setLevelRange(null);

		return levelTypeCharacterGame;
	}

}