package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GROUP_MEMBER database table.
 *
 */
@Entity
@Table(name = "GROUP_MEMBER")
@NamedQuery(name = "GroupMember.findAll", query = "SELECT g FROM GroupMember g")
public class GroupMember extends GenericEntity<GroupMember> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GROUP_MEMBER", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGroupMember;

	@Column(name = "FLG_CHAT_EMAIL")
	private byte[] flgChatEmail;

	@Column(name = "FLG_FILLED", nullable = false)
	private byte[] flgFilled;

	@Column(name = "FLG_IS_LEADER")
	private byte[] flgIsLeader;

	@Column(name = "FLG_IS_MANAGER")
	private byte[] flgIsManager;

	@Column(name = "FLG_PUBLIC_VACANCY", nullable = false)
	private byte[] flgPublicVacancy;

	// bi-directional many-to-one association to CharacterUserGame
	@ManyToOne
	@JoinColumn(name = "ID_CHARACTER_USER_GAME")
	private CharacterUserGame characterUserGame;

	// bi-directional many-to-one association to Group
	@ManyToOne
	@JoinColumn(name = "ID_GROUP", nullable = false)
	private Group group;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "ID_USER")
	private User user;

	// bi-directional many-to-one association to Todo_CRITERIA
	@OneToMany(mappedBy = "groupMember")
	private List<Todo_CRITERIA> todoCriterias;

	public GroupMember() {
	}

	public int getIdGroupMember() {
		return this.idGroupMember;
	}

	public void setIdGroupMember(int idGroupMember) {
		this.idGroupMember = idGroupMember;
	}

	public byte[] getFlgChatEmail() {
		return this.flgChatEmail;
	}

	public void setFlgChatEmail(byte[] flgChatEmail) {
		this.flgChatEmail = flgChatEmail;
	}

	public byte[] getFlgFilled() {
		return this.flgFilled;
	}

	public void setFlgFilled(byte[] flgFilled) {
		this.flgFilled = flgFilled;
	}

	public byte[] getFlgIsLeader() {
		return this.flgIsLeader;
	}

	public void setFlgIsLeader(byte[] flgIsLeader) {
		this.flgIsLeader = flgIsLeader;
	}

	public byte[] getFlgIsManager() {
		return this.flgIsManager;
	}

	public void setFlgIsManager(byte[] flgIsManager) {
		this.flgIsManager = flgIsManager;
	}

	public byte[] getFlgPublicVacancy() {
		return this.flgPublicVacancy;
	}

	public void setFlgPublicVacancy(byte[] flgPublicVacancy) {
		this.flgPublicVacancy = flgPublicVacancy;
	}

	public CharacterUserGame getCharacterUserGame() {
		return this.characterUserGame;
	}

	public void setCharacterUserGame(CharacterUserGame characterUserGame) {
		this.characterUserGame = characterUserGame;
	}

	public Group getGroup() {
		return this.group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Todo_CRITERIA> getTodoCriterias() {
		return this.todoCriterias;
	}

	public void setTodoCriterias(List<Todo_CRITERIA> todoCriterias) {
		this.todoCriterias = todoCriterias;
	}

	public Todo_CRITERIA addTodoCriteria(Todo_CRITERIA todoCriteria) {
		getTodoCriterias().add(todoCriteria);
		todoCriteria.setGroupMember(this);

		return todoCriteria;
	}

	public Todo_CRITERIA removeTodoCriteria(Todo_CRITERIA todoCriteria) {
		getTodoCriterias().remove(todoCriteria);
		todoCriteria.setGroupMember(null);

		return todoCriteria;
	}

}