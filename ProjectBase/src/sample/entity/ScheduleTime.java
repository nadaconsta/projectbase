package sample.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the SCHEDULE_TIME database table.
 *
 */
@Entity
@Table(name = "SCHEDULE_TIME")
@NamedQuery(name = "ScheduleTime.findAll", query = "SELECT s FROM ScheduleTime s")
public class ScheduleTime extends GenericEntity<ScheduleTime> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_SCHEDULE_TIME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idScheduleTime;

	@Column(name = "`DESC`", length = 255)
	private String desc;

	@Column(name = "TIME_BEGIN", nullable = false)
	private Date timeBegin;

	@Column(name = "TIME_END", nullable = false)
	private Date timeEnd;

	// bi-directional many-to-one association to Day
	@ManyToOne
	@JoinColumn(name = "ID_DAY", nullable = false)
	private Day day;

	// bi-directional many-to-one association to Schedule
	@ManyToOne
	@JoinColumn(name = "ID_SCHEDULE", nullable = false)
	private Schedule schedule;

	public ScheduleTime() {
	}

	public int getIdScheduleTime() {
		return this.idScheduleTime;
	}

	public void setIdScheduleTime(int idScheduleTime) {
		this.idScheduleTime = idScheduleTime;
	}

	public Date getTimeBegin() {
		return this.timeBegin;
	}

	public void setTimeBegin(Date timeBegin) {
		this.timeBegin = timeBegin;
	}

	public Date getTimeEnd() {
		return this.timeEnd;
	}

	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	public Day getDay() {
		return this.day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}