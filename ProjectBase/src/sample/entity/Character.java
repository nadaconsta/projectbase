package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the `CHARACTER` database table.
 *
 */
@Entity
@Table(name = "`CHARACTER`")
@NamedQuery(name = "Character.findAll", query = "SELECT c FROM Character c")
public class Character extends GenericEntity<Character> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_CHARACTER", unique = true, nullable = false, updatable = true, insertable = true)
	private int idCharacter;

	@Column(nullable = false, length = 255)
	private String name;

	@Lob
	private byte[] pic;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to CharacterUserGame
	@OneToMany(mappedBy = "character")
	private List<CharacterUserGame> characterUserGames;

	// bi-directional many-to-one association to GroupPatternCharacterRole
	@OneToMany(mappedBy = "character")
	private List<GroupPatternCharacterRole> groupPatternCharacterRoles;

	public Character() {
	}

	public int getIdCharacter() {
		return this.idCharacter;
	}

	public void setIdCharacter(int idCharacter) {
		this.idCharacter = idCharacter;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getPic() {
		return this.pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public List<CharacterUserGame> getCharacterUserGames() {
		return this.characterUserGames;
	}

	public void setCharacterUserGames(List<CharacterUserGame> characterUserGames) {
		this.characterUserGames = characterUserGames;
	}

	public CharacterUserGame addCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().add(characterUserGame);
		characterUserGame.setCharacter(this);

		return characterUserGame;
	}

	public CharacterUserGame removeCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().remove(characterUserGame);
		characterUserGame.setCharacter(null);

		return characterUserGame;
	}

	public List<GroupPatternCharacterRole> getGroupPatternCharacterRoles() {
		return this.groupPatternCharacterRoles;
	}

	public void setGroupPatternCharacterRoles(
			List<GroupPatternCharacterRole> groupPatternCharacterRoles) {
		this.groupPatternCharacterRoles = groupPatternCharacterRoles;
	}

	public GroupPatternCharacterRole addGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().add(groupPatternCharacterRole);
		groupPatternCharacterRole.setCharacter(this);

		return groupPatternCharacterRole;
	}

	public GroupPatternCharacterRole removeGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().remove(groupPatternCharacterRole);
		groupPatternCharacterRole.setCharacter(null);

		return groupPatternCharacterRole;
	}

}