package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_OPTION_CHARACTER database table.
 *
 */
@Entity
@Table(name = "LEVEL_OPTION_CHARACTER")
@NamedQuery(name = "LevelOptionCharacter.findAll", query = "SELECT l FROM LevelOptionCharacter l")
public class LevelOptionCharacter extends GenericEntity<LevelOptionCharacter>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_OPTION_ACCOUNT", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelOptionAccount;

	@Column(nullable = false, length = 255)
	private String value;

	// bi-directional many-to-one association to Level
	@OneToMany(mappedBy = "levelOptionCharacter")
	private List<Level> levels;

	// bi-directional many-to-one association to LevelTypeCharacterGame
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_TYPE_CHARACTER_GAME", nullable = false)
	private LevelTypeCharacterGame levelTypeCharacterGame;

	public LevelOptionCharacter() {
	}

	public int getIdLevelOptionAccount() {
		return this.idLevelOptionAccount;
	}

	public void setIdLevelOptionAccount(int idLevelOptionAccount) {
		this.idLevelOptionAccount = idLevelOptionAccount;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<Level> getLevels() {
		return this.levels;
	}

	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public Level addLevel(Level level) {
		getLevels().add(level);
		level.setLevelOptionCharacter(this);

		return level;
	}

	public Level removeLevel(Level level) {
		getLevels().remove(level);
		level.setLevelOptionCharacter(null);

		return level;
	}

	public LevelTypeCharacterGame getLevelTypeCharacterGame() {
		return this.levelTypeCharacterGame;
	}

	public void setLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		this.levelTypeCharacterGame = levelTypeCharacterGame;
	}

}