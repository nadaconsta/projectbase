package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the ACCOUNT_USER_GAME database table.
 * 
 */
@Entity
@Table(name = "ACCOUNT_USER_GAME")
@NamedQuery(name = "AccountUserGame.findAll", query = "SELECT a FROM AccountUserGame a")
public class AccountUserGame extends GenericEntity<AccountUserGame> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_ACCOUNT", unique = true, nullable = false, updatable = true, insertable = true)
	private int idAccount;

	@Column(name = "`DESC`", length = 255)
	private String desc;

	@Column(nullable = false, length = 255)
	private String name;

	// bi-directional many-to-one association to UserGame
	@ManyToOne
	@JoinColumn(name = "ID_USER_GAME", nullable = false)
	private UserGame userGame;

	// bi-directional many-to-one association to Server
	@ManyToOne
	@JoinColumn(name = "ID_SERVER", nullable = false)
	private Server server;

	// bi-directional many-to-one association to LevelAccountUserGame
	@OneToMany(mappedBy = "accountUserGame")
	private List<LevelAccountUserGame> levelAccountUserGames;

	public AccountUserGame() {
	}

	public int getIdAccount() {
		return this.idAccount;
	}

	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Server getServer() {
		return this.server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public UserGame getUserGame() {
		return this.userGame;
	}

	public void setUserGame(UserGame userGame) {
		this.userGame = userGame;
	}

	public List<LevelAccountUserGame> getLevelAccountUserGames() {
		return this.levelAccountUserGames;
	}

	public void setLevelAccountUserGames(
			List<LevelAccountUserGame> levelAccountUserGames) {
		this.levelAccountUserGames = levelAccountUserGames;
	}

	public LevelAccountUserGame addLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().add(levelAccountUserGame);
		levelAccountUserGame.setAccountUserGame(this);

		return levelAccountUserGame;
	}

	public LevelAccountUserGame removeLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().remove(levelAccountUserGame);
		levelAccountUserGame.setAccountUserGame(null);

		return levelAccountUserGame;
	}

}