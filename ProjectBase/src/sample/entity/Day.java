package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the DAY database table.
 *
 */
@Entity
@Table(name = "DAY")
@NamedQuery(name = "Day.findAll", query = "SELECT d FROM Day d")
public class Day extends GenericEntity<Day> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_DAY", unique = true, nullable = false, updatable = true, insertable = true)
	private int idDay;

	@Column(name = "`DESC`", nullable = false, length = 255)
	private String desc;

	// bi-directional many-to-one association to ScheduleTime
	@OneToMany(mappedBy = "day")
	private List<ScheduleTime> scheduleTimes;

	public Day() {
	}

	public int getIdDay() {
		return this.idDay;
	}

	public void setIdDay(int idDay) {
		this.idDay = idDay;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List<ScheduleTime> getScheduleTimes() {
		return this.scheduleTimes;
	}

	public void setScheduleTimes(List<ScheduleTime> scheduleTimes) {
		this.scheduleTimes = scheduleTimes;
	}

	public ScheduleTime addScheduleTime(ScheduleTime scheduleTime) {
		getScheduleTimes().add(scheduleTime);
		scheduleTime.setDay(this);

		return scheduleTime;
	}

	public ScheduleTime removeScheduleTime(ScheduleTime scheduleTime) {
		getScheduleTimes().remove(scheduleTime);
		scheduleTime.setDay(null);

		return scheduleTime;
	}

}