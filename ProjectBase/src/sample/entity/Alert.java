package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the ALERT database table.
 *
 */
@Entity
@Table(name = "ALERT")
@NamedQuery(name = "Alert.findAll", query = "SELECT a FROM Alert a")
public class Alert extends GenericEntity<Alert> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_ALERT", unique = true, nullable = false, updatable = true, insertable = true)
	private int idAlert;

	@Column(name = "FLG_EMAIL", nullable = false, length = 45)
	private String flgEmail;

	@Column(name = "TIME_BEFORE", nullable = false, length = 45)
	private String timeBefore;

	// bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy = "alert")
	private List<Schedule> schedules;

	public Alert() {
	}

	public int getIdAlert() {
		return this.idAlert;
	}

	public void setIdAlert(int idAlert) {
		this.idAlert = idAlert;
	}

	public String getFlgEmail() {
		return this.flgEmail;
	}

	public void setFlgEmail(String flgEmail) {
		this.flgEmail = flgEmail;
	}

	public String getTimeBefore() {
		return this.timeBefore;
	}

	public void setTimeBefore(String timeBefore) {
		this.timeBefore = timeBefore;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setAlert(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setAlert(null);

		return schedule;
	}

}