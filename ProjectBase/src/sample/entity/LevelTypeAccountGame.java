package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_TYPE_ACCOUNT_GAME database table.
 *
 */
@Entity
@Table(name = "LEVEL_TYPE_ACCOUNT_GAME")
@NamedQuery(name = "LevelTypeAccountGame.findAll", query = "SELECT l FROM LevelTypeAccountGame l")
public class LevelTypeAccountGame extends GenericEntity<LevelTypeAccountGame>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_TYPE_ACCOUNT_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelTypeAccountGame;

	@Column(name = "NUM_ORDER", nullable = false)
	private int numOrder;

	@Column(name = "TYPE_LVL", nullable = false, length = 255)
	private String typeLvl;

	// bi-directional many-to-one association to LevelAccountUserGame
	@OneToMany(mappedBy = "levelTypeAccountGame")
	private List<LevelAccountUserGame> levelAccountUserGames;

	// bi-directional many-to-one association to LevelOptionAccount
	@OneToMany(mappedBy = "levelTypeAccountGame")
	private List<LevelOptionAccount> levelOptionAccounts;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to LevelRange
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_RANGE", nullable = false)
	private LevelRange levelRange;

	public LevelTypeAccountGame() {
	}

	public int getIdLevelTypeAccountGame() {
		return this.idLevelTypeAccountGame;
	}

	public void setIdLevelTypeAccountGame(int idLevelTypeAccountGame) {
		this.idLevelTypeAccountGame = idLevelTypeAccountGame;
	}

	public int getNumOrder() {
		return this.numOrder;
	}

	public void setNumOrder(int numOrder) {
		this.numOrder = numOrder;
	}

	public String getTypeLvl() {
		return this.typeLvl;
	}

	public void setTypeLvl(String typeLvl) {
		this.typeLvl = typeLvl;
	}

	public List<LevelAccountUserGame> getLevelAccountUserGames() {
		return this.levelAccountUserGames;
	}

	public void setLevelAccountUserGames(
			List<LevelAccountUserGame> levelAccountUserGames) {
		this.levelAccountUserGames = levelAccountUserGames;
	}

	public LevelAccountUserGame addLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().add(levelAccountUserGame);
		levelAccountUserGame.setLevelTypeAccountGame(this);

		return levelAccountUserGame;
	}

	public LevelAccountUserGame removeLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().remove(levelAccountUserGame);
		levelAccountUserGame.setLevelTypeAccountGame(null);

		return levelAccountUserGame;
	}

	public List<LevelOptionAccount> getLevelOptionAccounts() {
		return this.levelOptionAccounts;
	}

	public void setLevelOptionAccounts(
			List<LevelOptionAccount> levelOptionAccounts) {
		this.levelOptionAccounts = levelOptionAccounts;
	}

	public LevelOptionAccount addLevelOptionAccount(
			LevelOptionAccount levelOptionAccount) {
		getLevelOptionAccounts().add(levelOptionAccount);
		levelOptionAccount.setLevelTypeAccountGame(this);

		return levelOptionAccount;
	}

	public LevelOptionAccount removeLevelOptionAccount(
			LevelOptionAccount levelOptionAccount) {
		getLevelOptionAccounts().remove(levelOptionAccount);
		levelOptionAccount.setLevelTypeAccountGame(null);

		return levelOptionAccount;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public LevelRange getLevelRange() {
		return this.levelRange;
	}

	public void setLevelRange(LevelRange levelRange) {
		this.levelRange = levelRange;
	}

}