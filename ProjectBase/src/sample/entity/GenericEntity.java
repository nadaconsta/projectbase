package sample.entity;

import java.lang.reflect.Method;

public class GenericEntity<T> {
	private Boolean selected;
	private String description;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		try {
			String idMethodName = "getId" + this.getClass().getSimpleName();
			Method[] methods = this.getClass().getDeclaredMethods();
			for (Method method : methods) {
				if (method.getName().compareTo(idMethodName) == 0) {
					return (Integer) method.invoke(this);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setId(Integer id) {
		try {
			String idMethodName = "setId" + this.getClass().getSimpleName();
			Method[] methods = this.getClass().getDeclaredMethods();
			for (Method method : methods) {
				if (method.getName().compareTo(idMethodName) == 0) {
					method.invoke(this, id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object entity) {
		if (this.getClass().equals(entity.getClass())
				&& this.getId().equals(((GenericEntity<T>) entity).getId())) {
			return true;
		} else {
			return false;
		}
	}

}
