package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the GROUP_PATTERN_CHARACTER_ROLE database table.
 *
 */
@Entity
@Table(name = "GROUP_PATTERN_CHARACTER_ROLE")
@NamedQuery(name = "GroupPatternCharacterRole.findAll", query = "SELECT g FROM GroupPatternCharacterRole g")
public class GroupPatternCharacterRole extends
		GenericEntity<GroupPatternCharacterRole> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GROUP_PATTERN_CHARACTER_ROLE", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGroupPatternCharacterRole;

	@Column(name = "NUM_CHARACTER")
	private int numCharacter;

	@Column(name = "NUM_ROLE")
	private int numRole;

	// bi-directional many-to-one association to Character
	@ManyToOne
	@JoinColumn(name = "ID_CHARACTER")
	private Character character;

	// bi-directional many-to-one association to GroupPattern
	@ManyToOne
	@JoinColumn(name = "ID_GROUP_PATTERN", nullable = false)
	private GroupPattern groupPattern;

	// bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "ID_ROLE")
	private Role role;

	public GroupPatternCharacterRole() {
	}

	public int getIdGroupPatternCharacterRole() {
		return this.idGroupPatternCharacterRole;
	}

	public void setIdGroupPatternCharacterRole(int idGroupPatternCharacterRole) {
		this.idGroupPatternCharacterRole = idGroupPatternCharacterRole;
	}

	public int getNumCharacter() {
		return this.numCharacter;
	}

	public void setNumCharacter(int numCharacter) {
		this.numCharacter = numCharacter;
	}

	public int getNumRole() {
		return this.numRole;
	}

	public void setNumRole(int numRole) {
		this.numRole = numRole;
	}

	public Character getCharacter() {
		return this.character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	public GroupPattern getGroupPattern() {
		return this.groupPattern;
	}

	public void setGroupPattern(GroupPattern groupPattern) {
		this.groupPattern = groupPattern;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}