package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the PROFICIENCY database table.
 *
 */
@Entity
@Table(name = "PROFICIENCY")
@NamedQueries({
		@NamedQuery(name = "Proficiency.findAll", query = "SELECT p FROM Proficiency p"),
		@NamedQuery(name = "Proficiency.findByType", query = "SELECT p FROM Proficiency p WHERE p.type = :type ORDER BY p.numOrder") })
public class Proficiency extends GenericEntity<Proficiency> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PROFICIENCY", unique = true, nullable = false, updatable = true, insertable = true)
	private int idProficiency;

	@Column(name = "`DESC`", nullable = false, length = 255)
	private String desc;

	@Column(name = "`NUM_ORDER`", nullable = false)
	private int numOrder;

	@Column(nullable = false, length = 5)
	private String type;

	// bi-directional many-to-one association to CharacterUserGame
	@OneToMany(mappedBy = "proficiency")
	private List<CharacterUserGame> characterUserGames;

	// bi-directional many-to-one association to Group
	@OneToMany(mappedBy = "proficiency")
	private List<Group> groups;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "proficiency")
	private List<User> users;

	// bi-directional many-to-one association to UserGame
	@OneToMany(mappedBy = "proficiency")
	private List<UserGame> userGames;

	public Proficiency() {
	}

	public int getIdProficiency() {
		return this.idProficiency;
	}

	public void setIdProficiency(int idProficiency) {
		this.idProficiency = idProficiency;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getNumOrder() {
		return this.numOrder;
	}

	public void setNumOrder(int numOrder) {
		this.numOrder = numOrder;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<CharacterUserGame> getCharacterUserGames() {
		return this.characterUserGames;
	}

	public void setCharacterUserGames(List<CharacterUserGame> characterUserGames) {
		this.characterUserGames = characterUserGames;
	}

	public CharacterUserGame addCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().add(characterUserGame);
		characterUserGame.setProficiency(this);

		return characterUserGame;
	}

	public CharacterUserGame removeCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().remove(characterUserGame);
		characterUserGame.setProficiency(null);

		return characterUserGame;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Group addGroup(Group group) {
		getGroups().add(group);
		group.setProficiency(this);

		return group;
	}

	public Group removeGroup(Group group) {
		getGroups().remove(group);
		group.setProficiency(null);

		return group;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setProficiency(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setProficiency(null);

		return user;
	}

	public List<UserGame> getUserGames() {
		return this.userGames;
	}

	public void setUserGames(List<UserGame> userGames) {
		this.userGames = userGames;
	}

	public UserGame addUserGame(UserGame userGame) {
		getUserGames().add(userGame);
		userGame.setProficiency(this);

		return userGame;
	}

	public UserGame removeUserGame(UserGame userGame) {
		getUserGames().remove(userGame);
		userGame.setProficiency(null);

		return userGame;
	}

}