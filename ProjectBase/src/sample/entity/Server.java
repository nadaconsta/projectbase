package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the SERVER database table.
 * 
 */
@Entity
@Table(name = "SERVER")
@NamedQuery(name = "Server.findAll", query = "SELECT s FROM Server s")
public class Server extends GenericEntity<Server> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_SERVER", unique = true, nullable = false, updatable = true, insertable = true)
	private int idServer;

	@Column(name = "`DESC`", nullable = false, length = 255)
	private String desc;

	@Column(nullable = false, length = 255)
	private String name;

	// bi-directional many-to-one association to CharacterUserGame
	@OneToMany(mappedBy = "server")
	private List<CharacterUserGame> characterUserGames;

	// bi-directional many-to-one association to AccountUserGame
	@OneToMany(mappedBy = "server")
	private List<AccountUserGame> accountUserGames;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	public Server() {
	}

	public int getIdServer() {
		return this.idServer;
	}

	public void setIdServer(int idServer) {
		this.idServer = idServer;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CharacterUserGame> getCharacterUserGames() {
		return this.characterUserGames;
	}

	public void setCharacterUserGames(List<CharacterUserGame> characterUserGames) {
		this.characterUserGames = characterUserGames;
	}

	public CharacterUserGame addCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().add(characterUserGame);
		characterUserGame.setServer(this);

		return characterUserGame;
	}

	public CharacterUserGame removeCharacterUserGame(
			CharacterUserGame characterUserGame) {
		getCharacterUserGames().remove(characterUserGame);
		characterUserGame.setServer(null);

		return characterUserGame;
	}

	public List<AccountUserGame> getAccountUserGames() {
		return this.accountUserGames;
	}

	public void setAccountUserGames(List<AccountUserGame> accountUserGames) {
		this.accountUserGames = accountUserGames;
	}

	public AccountUserGame addAccountUserGame(AccountUserGame accountUserGame) {
		getAccountUserGames().add(accountUserGame);
		accountUserGame.setServer(this);

		return accountUserGame;
	}

	public AccountUserGame removeAccountUserGame(AccountUserGame accountUserGame) {
		getAccountUserGames().remove(accountUserGame);
		accountUserGame.setServer(null);

		return accountUserGame;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

}