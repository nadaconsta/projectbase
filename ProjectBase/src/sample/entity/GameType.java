package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GAME_TYPE database table.
 *
 */
@Entity
@Table(name = "GAME_TYPE")
@NamedQuery(name = "GameType.findAll", query = "SELECT g FROM GameType g")
public class GameType extends GenericEntity<GameType> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GAME_TYPE", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGameType;

	@Column(name = "FLG_ACCOUNT_LEVEL", nullable = false)
	private byte[] flgAccountLevel;

	@Column(name = "FLG_ACCOUNT_SERVER", nullable = false)
	private byte[] flgAccountServer;

	@Column(name = "FLG_CHARACTER_LEVEL", nullable = false)
	private byte[] flgCharacterLevel;

	@Column(name = "FLG_CHARACTER_ROLE", nullable = false)
	private byte[] flgCharacterRole;

	@Column(name = "FLG_CHARACTER_SERVER", nullable = false)
	private byte[] flgCharacterServer;

	@Column(name = "FLG_CUSTOM_GROUP", nullable = false)
	private byte[] flgCustomGroup;

	@Column(name = "NUM_CHARACTER_LEVEL", nullable = false)
	private int numCharacterLevel;

	// bi-directional many-to-one association to Game
	@OneToMany(mappedBy = "gameType")
	private List<Game> games;

	public GameType() {
	}

	public int getIdGameType() {
		return this.idGameType;
	}

	public void setIdGameType(int idGameType) {
		this.idGameType = idGameType;
	}

	public byte[] getFlgAccountLevel() {
		return this.flgAccountLevel;
	}

	public void setFlgAccountLevel(byte[] flgAccountLevel) {
		this.flgAccountLevel = flgAccountLevel;
	}

	public byte[] getFlgAccountServer() {
		return this.flgAccountServer;
	}

	public void setFlgAccountServer(byte[] flgAccountServer) {
		this.flgAccountServer = flgAccountServer;
	}

	public byte[] getFlgCharacterLevel() {
		return this.flgCharacterLevel;
	}

	public void setFlgCharacterLevel(byte[] flgCharacterLevel) {
		this.flgCharacterLevel = flgCharacterLevel;
	}

	public byte[] getFlgCharacterRole() {
		return this.flgCharacterRole;
	}

	public void setFlgCharacterRole(byte[] flgCharacterRole) {
		this.flgCharacterRole = flgCharacterRole;
	}

	public byte[] getFlgCharacterServer() {
		return this.flgCharacterServer;
	}

	public void setFlgCharacterServer(byte[] flgCharacterServer) {
		this.flgCharacterServer = flgCharacterServer;
	}

	public byte[] getFlgCustomGroup() {
		return this.flgCustomGroup;
	}

	public void setFlgCustomGroup(byte[] flgCustomGroup) {
		this.flgCustomGroup = flgCustomGroup;
	}

	public int getNumCharacterLevel() {
		return this.numCharacterLevel;
	}

	public void setNumCharacterLevel(int numCharacterLevel) {
		this.numCharacterLevel = numCharacterLevel;
	}

	public List<Game> getGames() {
		return this.games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public Game addGame(Game game) {
		getGames().add(game);
		game.setGameType(this);

		return game;
	}

	public Game removeGame(Game game) {
		getGames().remove(game);
		game.setGameType(null);

		return game;
	}

}