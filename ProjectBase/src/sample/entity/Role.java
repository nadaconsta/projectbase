package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the ROLE database table.
 *
 */
@Entity
@Table(name = "ROLE")
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role extends GenericEntity<Role> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_ROLE", unique = true, nullable = false, updatable = true, insertable = true)
	private int idRole;

	@Column(nullable = false, length = 5)
	private String acronym;

	@Column(nullable = false, length = 255)
	private String name;

	@Lob
	private byte[] pic;

	// bi-directional many-to-one association to CharacterUserGame
	@OneToMany(mappedBy = "role")
	private List<CharacterUserGame> characterUserGameRoles;

	// bi-directional many-to-one association to GroupPatternCharacterRole
	@OneToMany(mappedBy = "role")
	private List<GroupPatternCharacterRole> groupPatternCharacterRoles;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	public Role() {
	}

	public int getIdRole() {
		return this.idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public String getAcronym() {
		return this.acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getPic() {
		return this.pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public List<CharacterUserGame> getCharacterUserGames() {
		return this.characterUserGameRoles;
	}

	public void setCharacterUserGames(
			List<CharacterUserGame> characterUserGameRoles) {
		this.characterUserGameRoles = characterUserGameRoles;
	}

	public CharacterUserGame addCharacterUserGame(
			CharacterUserGame characterUserGameRole) {
		getCharacterUserGames().add(characterUserGameRole);
		characterUserGameRole.setRole(this);

		return characterUserGameRole;
	}

	public CharacterUserGame removeCharacterUserGame(
			CharacterUserGame characterUserGameRole) {
		getCharacterUserGames().remove(characterUserGameRole);
		characterUserGameRole.setRole(null);

		return characterUserGameRole;
	}

	public List<GroupPatternCharacterRole> getGroupPatternCharacterRoles() {
		return this.groupPatternCharacterRoles;
	}

	public void setGroupPatternCharacterRoles(
			List<GroupPatternCharacterRole> groupPatternCharacterRoles) {
		this.groupPatternCharacterRoles = groupPatternCharacterRoles;
	}

	public GroupPatternCharacterRole addGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().add(groupPatternCharacterRole);
		groupPatternCharacterRole.setRole(this);

		return groupPatternCharacterRole;
	}

	public GroupPatternCharacterRole removeGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().remove(groupPatternCharacterRole);
		groupPatternCharacterRole.setRole(null);

		return groupPatternCharacterRole;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

}