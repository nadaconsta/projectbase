package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the SCHEDULE database table.
 * 
 */
@Entity
@Table(name = "SCHEDULE")
@NamedQuery(name = "Schedule.findAll", query = "SELECT s FROM Schedule s")
public class Schedule extends GenericEntity<Schedule> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_SCHEDULE", unique = true, nullable = false)
	private int idSchedule;

	// bi-directional many-to-one association to Group
	@OneToOne(mappedBy = "schedule")
	private User user;

	// bi-directional many-to-one association to Group
	@OneToOne(mappedBy = "schedule")
	private UserGame userGame;

	// bi-directional many-to-one association to Group
	@OneToMany(mappedBy = "schedule")
	private List<Group> groups;

	// bi-directional many-to-one association to Alert
	@ManyToOne
	@JoinColumn(name = "ID_ALERT")
	private Alert alert;

	// bi-directional many-to-one association to ScheduleTime
	@OneToMany(mappedBy = "schedule")
	private List<ScheduleTime> scheduleTimes;

	public Schedule() {
	}

	public int getIdSchedule() {
		return this.idSchedule;
	}

	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Group addGroup(Group group) {
		getGroups().add(group);
		group.setSchedule(this);

		return group;
	}

	public Group removeGroup(Group group) {
		getGroups().remove(group);
		group.setSchedule(null);

		return group;
	}

	public Alert getAlert() {
		return this.alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	public List<ScheduleTime> getScheduleTimes() {
		return this.scheduleTimes;
	}

	public void setScheduleTimes(List<ScheduleTime> scheduleTimes) {
		this.scheduleTimes = scheduleTimes;
	}

	public ScheduleTime addScheduleTime(ScheduleTime scheduleTime) {
		getScheduleTimes().add(scheduleTime);
		scheduleTime.setSchedule(this);

		return scheduleTime;
	}

	public ScheduleTime removeScheduleTime(ScheduleTime scheduleTime) {
		getScheduleTimes().remove(scheduleTime);
		scheduleTime.setSchedule(null);

		return scheduleTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserGame getUserGame() {
		return userGame;
	}

	public void setUserGame(UserGame userGame) {
		this.userGame = userGame;
	}

}