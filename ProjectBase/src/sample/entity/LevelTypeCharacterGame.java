package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_TYPE_CHARACTER_GAME database table.
 *
 */
@Entity
@Table(name = "LEVEL_TYPE_CHARACTER_GAME")
@NamedQuery(name = "LevelTypeCharacterGame.findAll", query = "SELECT l FROM LevelTypeCharacterGame l")
public class LevelTypeCharacterGame extends
		GenericEntity<LevelTypeCharacterGame> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_TYPE_CHARACTER_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelTypeCharacterGame;

	@Column(name = "NUM_ORDER", nullable = false)
	private int numOrder;

	@Column(name = "TYPE_LEVEL", nullable = false, length = 50)
	private String typeLevel;

	// bi-directional many-to-one association to LevelCharacterUserGame
	@OneToMany(mappedBy = "levelTypeCharacterGame")
	private List<LevelCharacterUserGame> levelCharacterUserGames;

	// bi-directional many-to-one association to LevelOptionCharacter
	@OneToMany(mappedBy = "levelTypeCharacterGame")
	private List<LevelOptionCharacter> levelOptionCharacters;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to LevelRange
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_RANGE", nullable = false)
	private LevelRange levelRange;

	public LevelTypeCharacterGame() {
	}

	public int getIdLevelTypeCharacterGame() {
		return this.idLevelTypeCharacterGame;
	}

	public void setIdLevelTypeCharacterGame(int idLevelTypeCharacterGame) {
		this.idLevelTypeCharacterGame = idLevelTypeCharacterGame;
	}

	public int getNumOrder() {
		return this.numOrder;
	}

	public void setNumOrder(int numOrder) {
		this.numOrder = numOrder;
	}

	public String getTypeLevel() {
		return this.typeLevel;
	}

	public void setTypeLevel(String typeLevel) {
		this.typeLevel = typeLevel;
	}

	public List<LevelCharacterUserGame> getLevelCharacterUserGames() {
		return this.levelCharacterUserGames;
	}

	public void setLevelCharacterUserGames(
			List<LevelCharacterUserGame> levelCharacterUserGames) {
		this.levelCharacterUserGames = levelCharacterUserGames;
	}

	public LevelCharacterUserGame addLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().add(levelCharacterUserGame);
		levelCharacterUserGame.setLevelTypeCharacterGame(this);

		return levelCharacterUserGame;
	}

	public LevelCharacterUserGame removeLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().remove(levelCharacterUserGame);
		levelCharacterUserGame.setLevelTypeCharacterGame(null);

		return levelCharacterUserGame;
	}

	public List<LevelOptionCharacter> getLevelOptionCharacters() {
		return this.levelOptionCharacters;
	}

	public void setLevelOptionCharacters(
			List<LevelOptionCharacter> levelOptionCharacters) {
		this.levelOptionCharacters = levelOptionCharacters;
	}

	public LevelOptionCharacter addLevelOptionCharacter(
			LevelOptionCharacter levelOptionCharacter) {
		getLevelOptionCharacters().add(levelOptionCharacter);
		levelOptionCharacter.setLevelTypeCharacterGame(this);

		return levelOptionCharacter;
	}

	public LevelOptionCharacter removeLevelOptionCharacter(
			LevelOptionCharacter levelOptionCharacter) {
		getLevelOptionCharacters().remove(levelOptionCharacter);
		levelOptionCharacter.setLevelTypeCharacterGame(null);

		return levelOptionCharacter;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public LevelRange getLevelRange() {
		return this.levelRange;
	}

	public void setLevelRange(LevelRange levelRange) {
		this.levelRange = levelRange;
	}

}