package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the DEVELOPER database table.
 *
 */
@Entity
@Table(name = "DEVELOPER")
@NamedQuery(name = "Developer.findAll", query = "SELECT d FROM Developer d")
public class Developer extends GenericEntity<Developer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_DEVELOPER", unique = true, nullable = false, updatable = true, insertable = true)
	private int idDeveloper;

	@Column(nullable = false, length = 255)
	private String name;

	@Column(nullable = false, length = 255)
	private String website;

	// bi-directional many-to-one association to Game
	@OneToMany(mappedBy = "developer")
	private List<Game> games;

	public Developer() {
	}

	public int getIdDeveloper() {
		return this.idDeveloper;
	}

	public void setIdDeveloper(int idDeveloper) {
		this.idDeveloper = idDeveloper;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public List<Game> getGames() {
		return this.games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public Game addGame(Game game) {
		getGames().add(game);
		game.setDeveloper(this);

		return game;
	}

	public Game removeGame(Game game) {
		getGames().remove(game);
		game.setDeveloper(null);

		return game;
	}

}