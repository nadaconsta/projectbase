package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL_CHARACTER_USER_GAME database table.
 *
 */
@Entity
@Table(name = "LEVEL_CHARACTER_USER_GAME")
@NamedQuery(name = "LevelCharacterUserGame.findAll", query = "SELECT l FROM LevelCharacterUserGame l")
public class LevelCharacterUserGame extends
		GenericEntity<LevelCharacterUserGame> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL_CHARACTER_USER_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevelCharacterUserGame;

	// bi-directional many-to-one association to CharacterUserGame
	@ManyToOne
	@JoinColumn(name = "ID_CHARACTER_USER_GAME", nullable = false)
	private CharacterUserGame characterUserGame;

	// bi-directional many-to-one association to Level
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL", nullable = false)
	private Level level;

	// bi-directional many-to-one association to LevelTypeCharacterGame
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_TYPE_CHARACTER_GAME", nullable = false)
	private LevelTypeCharacterGame levelTypeCharacterGame;

	public LevelCharacterUserGame() {
	}

	public int getIdLevelCharacterUserGame() {
		return this.idLevelCharacterUserGame;
	}

	public void setIdLevelCharacterUserGame(int idLevelCharacterUserGame) {
		this.idLevelCharacterUserGame = idLevelCharacterUserGame;
	}

	public CharacterUserGame getCharacterUserGame() {
		return this.characterUserGame;
	}

	public void setCharacterUserGame(CharacterUserGame characterUserGame) {
		this.characterUserGame = characterUserGame;
	}

	public Level getLevel() {
		return this.level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public LevelTypeCharacterGame getLevelTypeCharacterGame() {
		return this.levelTypeCharacterGame;
	}

	public void setLevelTypeCharacterGame(
			LevelTypeCharacterGame levelTypeCharacterGame) {
		this.levelTypeCharacterGame = levelTypeCharacterGame;
	}

}