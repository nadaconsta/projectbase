package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the `GROUP` database table.
 *
 */
@Entity
@Table(name = "`GROUP`")
@NamedQuery(name = "Group.findAll", query = "SELECT g FROM Group g")
public class Group extends GenericEntity<Group> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GROUP", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGroup;

	@Column(length = 255)
	private String comunication;

	@Column(name = "`DESC`", nullable = false, length = 255)
	private String desc;

	@Column(name = "FLG_PUBLIC_MESSAGES", nullable = false)
	private byte[] flgPublicMessages;

	@Column(nullable = false, length = 255)
	private String name;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to GroupPattern
	@ManyToOne
	@JoinColumn(name = "ID_GROUP_PATTERN")
	private GroupPattern groupPattern;

	// bi-directional many-to-one association to Proficiency
	@ManyToOne
	@JoinColumn(name = "ID_PROFICIENCY", nullable = false)
	private Proficiency proficiency;

	// bi-directional many-to-one association to Schedule
	@ManyToOne
	@JoinColumn(name = "ID_SCHEDULE", nullable = false)
	private Schedule schedule;

	// bi-directional many-to-one association to GroupMember
	@OneToMany(mappedBy = "group")
	private List<GroupMember> groupMembers;

	// bi-directional many-to-one association to GroupPermission
	@OneToMany(mappedBy = "group")
	private List<GroupPermission> groupPermissions;

	public Group() {
	}

	public int getIdGroup() {
		return this.idGroup;
	}

	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}

	public String getComunication() {
		return this.comunication;
	}

	public void setComunication(String comunication) {
		this.comunication = comunication;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public byte[] getFlgPublicMessages() {
		return this.flgPublicMessages;
	}

	public void setFlgPublicMessages(byte[] flgPublicMessages) {
		this.flgPublicMessages = flgPublicMessages;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public GroupPattern getGroupPattern() {
		return this.groupPattern;
	}

	public void setGroupPattern(GroupPattern groupPattern) {
		this.groupPattern = groupPattern;
	}

	public Proficiency getProficiency() {
		return this.proficiency;
	}

	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public List<GroupMember> getGroupMembers() {
		return this.groupMembers;
	}

	public void setGroupMembers(List<GroupMember> groupMembers) {
		this.groupMembers = groupMembers;
	}

	public GroupMember addGroupMember(GroupMember groupMember) {
		getGroupMembers().add(groupMember);
		groupMember.setGroup(this);

		return groupMember;
	}

	public GroupMember removeGroupMember(GroupMember groupMember) {
		getGroupMembers().remove(groupMember);
		groupMember.setGroup(null);

		return groupMember;
	}

	public List<GroupPermission> getGroupPermissions() {
		return this.groupPermissions;
	}

	public void setGroupPermissions(List<GroupPermission> groupPermissions) {
		this.groupPermissions = groupPermissions;
	}

	public GroupPermission addGroupPermission(GroupPermission groupPermission) {
		getGroupPermissions().add(groupPermission);
		groupPermission.setGroup(this);

		return groupPermission;
	}

	public GroupPermission removeGroupPermission(GroupPermission groupPermission) {
		getGroupPermissions().remove(groupPermission);
		groupPermission.setGroup(null);

		return groupPermission;
	}

}