package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the GROUP_PERMISSION database table.
 *
 */
@Entity
@Table(name = "GROUP_PERMISSION")
@NamedQuery(name = "GroupPermission.findAll", query = "SELECT g FROM GroupPermission g")
public class GroupPermission extends GenericEntity<GroupPermission> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GROUP_PERMISSION", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGroupPermission;

	@Column(name = "FLG_ADD_MEMBER", nullable = false)
	private byte[] flgAddMember;

	@Column(name = "FLG_CLOSE_VACANCY", nullable = false)
	private byte[] flgCloseVacancy;

	@Column(name = "FLG_DEMOTE_MEMBER", nullable = false)
	private byte[] flgDemoteMember;

	@Column(name = "FLG_EDIT_GENERAL", nullable = false)
	private byte[] flgEditGeneral;

	@Column(name = "FLG_EDIT_SCHEDULE", nullable = false)
	private byte[] flgEditSchedule;

	@Column(name = "FLG_OPEN_VACANCY", nullable = false)
	private byte[] flgOpenVacancy;

	@Column(name = "FLG_PROMOTE_MEMBER", nullable = false)
	private byte[] flgPromoteMember;

	@Column(name = "FLG_REMOVE_MEMBER", nullable = false)
	private byte[] flgRemoveMember;

	// bi-directional many-to-one association to Group
	@ManyToOne
	@JoinColumn(name = "ID_GROUP", nullable = false)
	private Group group;

	public GroupPermission() {
	}

	public int getIdGroupPermission() {
		return this.idGroupPermission;
	}

	public void setIdGroupPermission(int idGroupPermission) {
		this.idGroupPermission = idGroupPermission;
	}

	public byte[] getFlgAddMember() {
		return this.flgAddMember;
	}

	public void setFlgAddMember(byte[] flgAddMember) {
		this.flgAddMember = flgAddMember;
	}

	public byte[] getFlgCloseVacancy() {
		return this.flgCloseVacancy;
	}

	public void setFlgCloseVacancy(byte[] flgCloseVacancy) {
		this.flgCloseVacancy = flgCloseVacancy;
	}

	public byte[] getFlgDemoteMember() {
		return this.flgDemoteMember;
	}

	public void setFlgDemoteMember(byte[] flgDemoteMember) {
		this.flgDemoteMember = flgDemoteMember;
	}

	public byte[] getFlgEditGeneral() {
		return this.flgEditGeneral;
	}

	public void setFlgEditGeneral(byte[] flgEditGeneral) {
		this.flgEditGeneral = flgEditGeneral;
	}

	public byte[] getFlgEditSchedule() {
		return this.flgEditSchedule;
	}

	public void setFlgEditSchedule(byte[] flgEditSchedule) {
		this.flgEditSchedule = flgEditSchedule;
	}

	public byte[] getFlgOpenVacancy() {
		return this.flgOpenVacancy;
	}

	public void setFlgOpenVacancy(byte[] flgOpenVacancy) {
		this.flgOpenVacancy = flgOpenVacancy;
	}

	public byte[] getFlgPromoteMember() {
		return this.flgPromoteMember;
	}

	public void setFlgPromoteMember(byte[] flgPromoteMember) {
		this.flgPromoteMember = flgPromoteMember;
	}

	public byte[] getFlgRemoveMember() {
		return this.flgRemoveMember;
	}

	public void setFlgRemoveMember(byte[] flgRemoveMember) {
		this.flgRemoveMember = flgRemoveMember;
	}

	public Group getGroup() {
		return this.group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}