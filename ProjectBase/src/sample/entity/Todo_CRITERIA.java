package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the todo_CRITERIA database table.
 *
 */
@Entity
@Table(name = "todo_CRITERIA")
@NamedQuery(name = "Todo_CRITERIA.findAll", query = "SELECT t FROM Todo_CRITERIA t")
public class Todo_CRITERIA extends GenericEntity<Todo_CRITERIA> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_CRITERIA", unique = true, nullable = false, updatable = true, insertable = true)
	private int idCriteria;

	// bi-directional many-to-one association to GroupMember
	@ManyToOne
	@JoinColumn(name = "ID_GROUP_MEMBER", nullable = false)
	private GroupMember groupMember;

	public Todo_CRITERIA() {
	}

	public int getIdCriteria() {
		return this.idCriteria;
	}

	public void setIdCriteria(int idCriteria) {
		this.idCriteria = idCriteria;
	}

	public GroupMember getGroupMember() {
		return this.groupMember;
	}

	public void setGroupMember(GroupMember groupMember) {
		this.groupMember = groupMember;
	}

}