package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the USER database table.
 * 
 */
@Entity
@Table(name = "USER")
@NamedQueries({
		@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.idUser = :idUser"),
		@NamedQuery(name = "User.findByEmailPassword", query = "SELECT u FROM User u WHERE u.email = :email AND u.password = :password") })
public class User extends GenericEntity<User> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER", unique = true, nullable = false)
	private int idUser;

	@Column(nullable = false, length = 255)
	private String email;

	@Column(name = "NAME_FIRST", nullable = false, length = 255)
	private String nameFirst;

	@Column(name = "NAME_LAST", nullable = false, length = 255)
	private String nameLast;

	@Column(name = "NAME_NICK", nullable = false, length = 255)
	private String nameNick;

	@Column(nullable = false, length = 255)
	private String password;

	// bi-directional many-to-one association to GroupMember
	@OneToMany(mappedBy = "user")
	private List<GroupMember> groupMembers;

	// bi-directional many-to-one association to Proficiency
	@ManyToOne
	@JoinColumn(name = "ID_PROFICIENCY")
	private Proficiency proficiency;

	// bi-directional many-to-one association to Schedule
	@OneToOne
	@JoinColumn(name = "ID_SCHEDULE")
	private Schedule schedule;

	// bi-directional many-to-one association to UserGame
	@OneToMany(mappedBy = "user")
	private List<UserGame> userGames;

	public User() {
	}

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNameFirst() {
		return this.nameFirst;
	}

	public void setNameFirst(String nameFirst) {
		this.nameFirst = nameFirst;
	}

	public String getNameLast() {
		return this.nameLast;
	}

	public void setNameLast(String nameLast) {
		this.nameLast = nameLast;
	}

	public String getNameNick() {
		return this.nameNick;
	}

	public void setNameNick(String nameNick) {
		this.nameNick = nameNick;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<GroupMember> getGroupMembers() {
		return this.groupMembers;
	}

	public void setGroupMembers(List<GroupMember> groupMembers) {
		this.groupMembers = groupMembers;
	}

	public GroupMember addGroupMember(GroupMember groupMember) {
		getGroupMembers().add(groupMember);
		groupMember.setUser(this);

		return groupMember;
	}

	public GroupMember removeGroupMember(GroupMember groupMember) {
		getGroupMembers().remove(groupMember);
		groupMember.setUser(null);

		return groupMember;
	}

	public Proficiency getProficiency() {
		return this.proficiency;
	}

	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public List<UserGame> getUserGames() {
		return this.userGames;
	}

	public void setUserGames(List<UserGame> userGames) {
		this.userGames = userGames;
	}

	public UserGame addUserGame(UserGame userGame) {
		getUserGames().add(userGame);
		userGame.setUser(this);

		return userGame;
	}

	public UserGame removeUserGame(UserGame userGame) {
		getUserGames().remove(userGame);
		userGame.setUser(null);

		return userGame;
	}

}