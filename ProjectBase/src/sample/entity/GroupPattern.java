package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GROUP_PATTERN database table.
 *
 */
@Entity
@Table(name = "GROUP_PATTERN")
@NamedQuery(name = "GroupPattern.findAll", query = "SELECT g FROM GroupPattern g")
public class GroupPattern extends GenericEntity<GroupPattern> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_GROUP_PATTERN", unique = true, nullable = false, updatable = true, insertable = true)
	private int idGroupPattern;

	@Column(name = "`DESC`", nullable = false, length = 255)
	private String desc;

	// bi-directional many-to-one association to Group
	@OneToMany(mappedBy = "groupPattern")
	private List<Group> groups;

	// bi-directional many-to-one association to Game
	@ManyToOne
	@JoinColumn(name = "ID_GAME", nullable = false)
	private Game game;

	// bi-directional many-to-one association to GroupPatternCharacterRole
	@OneToMany(mappedBy = "groupPattern")
	private List<GroupPatternCharacterRole> groupPatternCharacterRoles;

	public GroupPattern() {
	}

	public int getIdGroupPattern() {
		return this.idGroupPattern;
	}

	public void setIdGroupPattern(int idGroupPattern) {
		this.idGroupPattern = idGroupPattern;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Group addGroup(Group group) {
		getGroups().add(group);
		group.setGroupPattern(this);

		return group;
	}

	public Group removeGroup(Group group) {
		getGroups().remove(group);
		group.setGroupPattern(null);

		return group;
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public List<GroupPatternCharacterRole> getGroupPatternCharacterRoles() {
		return this.groupPatternCharacterRoles;
	}

	public void setGroupPatternCharacterRoles(
			List<GroupPatternCharacterRole> groupPatternCharacterRoles) {
		this.groupPatternCharacterRoles = groupPatternCharacterRoles;
	}

	public GroupPatternCharacterRole addGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().add(groupPatternCharacterRole);
		groupPatternCharacterRole.setGroupPattern(this);

		return groupPatternCharacterRole;
	}

	public GroupPatternCharacterRole removeGroupPatternCharacterRole(
			GroupPatternCharacterRole groupPatternCharacterRole) {
		getGroupPatternCharacterRoles().remove(groupPatternCharacterRole);
		groupPatternCharacterRole.setGroupPattern(null);

		return groupPatternCharacterRole;
	}

}