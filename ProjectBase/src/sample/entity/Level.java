package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the LEVEL database table.
 *
 */
@Entity
@Table(name = "LEVEL")
@NamedQuery(name = "Level.findAll", query = "SELECT l FROM Level l")
public class Level extends GenericEntity<Level> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LEVEL", unique = true, nullable = false, updatable = true, insertable = true)
	private int idLevel;

	private int value;

	// bi-directional many-to-one association to LevelOptionAccount
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_OPTION_ACCOUNT")
	private LevelOptionAccount levelOptionAccount;

	// bi-directional many-to-one association to LevelOptionCharacter
	@ManyToOne
	@JoinColumn(name = "ID_LEVEL_OPTION_CHARACTER")
	private LevelOptionCharacter levelOptionCharacter;

	// bi-directional many-to-one association to LevelAccountUserGame
	@OneToMany(mappedBy = "level")
	private List<LevelAccountUserGame> levelAccountUserGames;

	// bi-directional many-to-one association to LevelCharacterUserGame
	@OneToMany(mappedBy = "level")
	private List<LevelCharacterUserGame> levelCharacterUserGames;

	public Level() {
	}

	public int getIdLevel() {
		return this.idLevel;
	}

	public void setIdLevel(int idLevel) {
		this.idLevel = idLevel;
	}

	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public LevelOptionAccount getLevelOptionAccount() {
		return this.levelOptionAccount;
	}

	public void setLevelOptionAccount(LevelOptionAccount levelOptionAccount) {
		this.levelOptionAccount = levelOptionAccount;
	}

	public LevelOptionCharacter getLevelOptionCharacter() {
		return this.levelOptionCharacter;
	}

	public void setLevelOptionCharacter(
			LevelOptionCharacter levelOptionCharacter) {
		this.levelOptionCharacter = levelOptionCharacter;
	}

	public List<LevelAccountUserGame> getLevelAccountUserGames() {
		return this.levelAccountUserGames;
	}

	public void setLevelAccountUserGames(
			List<LevelAccountUserGame> levelAccountUserGames) {
		this.levelAccountUserGames = levelAccountUserGames;
	}

	public LevelAccountUserGame addLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().add(levelAccountUserGame);
		levelAccountUserGame.setLevel(this);

		return levelAccountUserGame;
	}

	public LevelAccountUserGame removeLevelAccountUserGame(
			LevelAccountUserGame levelAccountUserGame) {
		getLevelAccountUserGames().remove(levelAccountUserGame);
		levelAccountUserGame.setLevel(null);

		return levelAccountUserGame;
	}

	public List<LevelCharacterUserGame> getLevelCharacterUserGames() {
		return this.levelCharacterUserGames;
	}

	public void setLevelCharacterUserGames(
			List<LevelCharacterUserGame> levelCharacterUserGames) {
		this.levelCharacterUserGames = levelCharacterUserGames;
	}

	public LevelCharacterUserGame addLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().add(levelCharacterUserGame);
		levelCharacterUserGame.setLevel(this);

		return levelCharacterUserGame;
	}

	public LevelCharacterUserGame removeLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().remove(levelCharacterUserGame);
		levelCharacterUserGame.setLevel(null);

		return levelCharacterUserGame;
	}

}