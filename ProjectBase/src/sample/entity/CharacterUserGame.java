package sample.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the CHARACTER_USER_GAME database table.
 * 
 */
@Entity
@Table(name = "CHARACTER_USER_GAME")
@NamedQuery(name = "CharacterUserGame.findAll", query = "SELECT c FROM CharacterUserGame c")
public class CharacterUserGame extends GenericEntity<CharacterUserGame>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_CHARACTER_USER_GAME", unique = true, nullable = false, updatable = true, insertable = true)
	private int idCharacterUserGame;

	// bi-directional many-to-one association to Character
	@ManyToOne
	@JoinColumn(name = "ID_CHARACTER", nullable = false)
	private Character character;

	// bi-directional many-to-one association to Proficiency
	@ManyToOne
	@JoinColumn(name = "ID_PROFICIENCY", nullable = false)
	private Proficiency proficiency;

	// bi-directional many-to-one association to Server
	@ManyToOne
	@JoinColumn(name = "ID_SERVER")
	private Server server;

	// bi-directional many-to-one association to UserGame
	@ManyToOne
	@JoinColumn(name = "ID_USER_GAME", nullable = false)
	private UserGame userGame;

	// bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "ID_ROLE")
	private Role role;

	// bi-directional many-to-one association to GroupMember
	@OneToMany(mappedBy = "characterUserGame")
	private List<GroupMember> groupMembers;

	// bi-directional many-to-one association to LevelCharacterUserGame
	@OneToMany(mappedBy = "characterUserGame")
	private List<LevelCharacterUserGame> levelCharacterUserGames;

	public CharacterUserGame() {
	}

	public int getIdCharacterUserGame() {
		return this.idCharacterUserGame;
	}

	public void setIdCharacterUserGame(int idCharacterUserGame) {
		this.idCharacterUserGame = idCharacterUserGame;
	}

	public Character getCharacter() {
		return this.character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	public Proficiency getProficiency() {
		return this.proficiency;
	}

	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}

	public Server getServer() {
		return this.server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public UserGame getUserGame() {
		return this.userGame;
	}

	public void setUserGame(UserGame userGame) {
		this.userGame = userGame;
	}

	public List<GroupMember> getGroupMembers() {
		return this.groupMembers;
	}

	public void setGroupMembers(List<GroupMember> groupMembers) {
		this.groupMembers = groupMembers;
	}

	public GroupMember addGroupMember(GroupMember groupMember) {
		getGroupMembers().add(groupMember);
		groupMember.setCharacterUserGame(this);

		return groupMember;
	}

	public GroupMember removeGroupMember(GroupMember groupMember) {
		getGroupMembers().remove(groupMember);
		groupMember.setCharacterUserGame(null);

		return groupMember;
	}

	public List<LevelCharacterUserGame> getLevelCharacterUserGames() {
		return this.levelCharacterUserGames;
	}

	public void setLevelCharacterUserGames(
			List<LevelCharacterUserGame> levelCharacterUserGames) {
		this.levelCharacterUserGames = levelCharacterUserGames;
	}

	public LevelCharacterUserGame addLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().add(levelCharacterUserGame);
		levelCharacterUserGame.setCharacterUserGame(this);

		return levelCharacterUserGame;
	}

	public LevelCharacterUserGame removeLevelCharacterUserGame(
			LevelCharacterUserGame levelCharacterUserGame) {
		getLevelCharacterUserGames().remove(levelCharacterUserGame);
		levelCharacterUserGame.setCharacterUserGame(null);

		return levelCharacterUserGame;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}