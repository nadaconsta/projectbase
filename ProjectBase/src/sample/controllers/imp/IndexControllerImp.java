package sample.controllers.imp;

import project.exceptions.WarnException;
import project.services.CreateService;
import project.services.ReadService;
import sample.controllers.HomeController;
import sample.controllers.IndexController;
import sample.controllers.UserRegisterController;
import sample.entity.User;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.NoResultException;

@ViewScoped
@ManagedBean(eager = true)
public class IndexControllerImp extends GenericControllerImp implements
		IndexController {
	User user;
	HomeController homeController;
	CreateService createService;
	ReadService readService;

	@Override
	@PostConstruct
	public void init() {
		user = new User();
		homeController = super.findBean(HomeController.class);
		createService = super.findBean(CreateService.class);
		readService = super.findBean(ReadService.class);
	}

	@Override
	public String actionLogin() {
		try {
			user = readService.readUserByEmailPassword(user);
			homeController.setUser(user);
			return homeController.getBeanName();
		} catch (NoResultException e) {
			super.sendMessage(new WarnException(770001));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectUserRegistration() {
		return getBeanName(UserRegisterController.class);
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}
}
