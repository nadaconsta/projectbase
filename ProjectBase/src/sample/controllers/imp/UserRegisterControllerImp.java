package sample.controllers.imp;

import project.services.CreateService;
import sample.controllers.UserRegisterController;
import sample.entity.User;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class UserRegisterControllerImp extends GenericControllerImp implements
		UserRegisterController {

	// Entities
	private User user;

	// Services
	private CreateService createService;

	@Override
	@PostConstruct
	public void init() {
		this.user = new User();
		this.createService = super.findBean(CreateService.class);
	}

	@Override
	public String actionSaveUser() {
		try {
			this.user = createService.createUser(this.user);
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectIndex() {
		return getBeanName(IndexControllerImp.class);
	}

	// Getters and Setters
	@Override
	public User getUser() {
		return this.user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

}