package sample.controllers.imp;

import project.constants.Constants;
import project.services.CreateService;
import project.services.DeleteService;
import project.services.UpdateService;
import sample.controllers.HomeController;
import sample.controllers.ScheduleCrudController;
import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.User;
import sample.entity.UserGame;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class ScheduleCrudControllerImp extends GenericControllerImp implements
		ScheduleCrudController {

	private HomeController homeController;

	private User user;
	private Schedule schedule;
	private ScheduleTime scheduleTime;

	// Services
	private CreateService createService;
	private UpdateService updateService;
	private DeleteService deleteService;

	// Attributes, remember to clean them

	private Map<Integer, Schedule> mpSchedules;
	private List<ScheduleTime> lsDeleteScheduleTime;
	private List<ScheduleTime> lsCreateScheduleTime;
	private List<ScheduleTime> lsUpdateScheduleTime;

	@Override
	@PostConstruct
	public void init() {
		createService = super.findBean(CreateService.class);
		updateService = super.findBean(UpdateService.class);
		deleteService = super.findBean(DeleteService.class);
		homeController = super.findBean(HomeController.class);

		this.user = homeController.getUser();
		this.fillSchedules();
	}

	@Override
	public String actionAddScheduleTime() {
		try {
			this.schedule = mpSchedules.get(schedule.getIdSchedule());
			this.scheduleTime = new ScheduleTime();
			this.schedule.addScheduleTime(scheduleTime);

		} catch (Exception e) {
			this.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String actionDelScheduleTime() {
		try {
			this.schedule = mpSchedules.get(schedule.getIdSchedule());

			this.schedule.removeScheduleTime(scheduleTime);
			if (scheduleTime.getIdScheduleTime() != Constants.ZERO_INT) {
				this.lsDeleteScheduleTime.add(scheduleTime);
			} else {
				this.schedule.removeScheduleTime(scheduleTime);
			}
			scheduleTime.setSchedule(schedule);
		} catch (Exception e) {
			this.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionSaveSchedule() {
		try {
			for (ScheduleTime scheduleTime : lsDeleteScheduleTime) {
				deleteService.deleteScheduleTime(scheduleTime);
			}
			lsDeleteScheduleTime = new ArrayList<ScheduleTime>();

			lsCreateScheduleTime = new ArrayList<ScheduleTime>();
			lsUpdateScheduleTime = new ArrayList<ScheduleTime>();
			for (Schedule schedule : this.mpSchedules.values()) {
				for (ScheduleTime scheduleTime : schedule.getScheduleTimes()) {
					if (scheduleTime.getIdScheduleTime() != Constants.ZERO_INT) {
						lsUpdateScheduleTime.add(scheduleTime);
					} else {
						lsCreateScheduleTime.add(scheduleTime);
					}
				}
			}
			for (ScheduleTime scheduleTime : lsUpdateScheduleTime) {
				updateService.updateScheduleTime(scheduleTime);
			}
			for (ScheduleTime scheduleTime : lsCreateScheduleTime) {
				createService.createScheduleTime(scheduleTime);
			}

			return this.getBeanName();
		} catch (Exception e) {
			this.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectHome() {
		return homeController.getBeanName();
	}

	private void fillSchedules() {
		mpSchedules = new LinkedHashMap<Integer, Schedule>();

		// General schedule, not for a specific game
		Schedule schedule = user.getSchedule();
		mpSchedules.put(schedule.getIdSchedule(), schedule);

		// A schedule for each game this user plays
		List<UserGame> lsUserGame = user.getUserGames();
		for (UserGame userGame : lsUserGame) {
			schedule = userGame.getSchedule();
			schedule.getScheduleTimes();
			mpSchedules.put(schedule.getIdSchedule(), schedule);
		}

		if (lsDeleteScheduleTime == null) {
			lsDeleteScheduleTime = new ArrayList<ScheduleTime>();
		}
	}

	// Getters and Setters
	@Override
	public List<Schedule> getSelectSchedules() {
		List<Schedule> lsSchedules = new ArrayList<Schedule>();
		lsSchedules.addAll(this.mpSchedules.values());
		return lsSchedules;
	}

	@Override
	public void setSelectSchedules(List<Schedule> lsSchedules) {
		this.mpSchedules = new LinkedHashMap<Integer, Schedule>();
		for(Schedule schedule : lsSchedules){
			this.mpSchedules.put(schedule.getIdSchedule(), schedule);
		}
	}

	@Override
	public Schedule getSchedule() {
		return schedule;
	}

	@Override
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	@Override
	public ScheduleTime getScheduleTime() {
		return scheduleTime;
	}

	@Override
	public void setScheduleTime(ScheduleTime scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

}
