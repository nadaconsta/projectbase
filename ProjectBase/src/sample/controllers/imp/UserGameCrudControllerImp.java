package sample.controllers.imp;

import project.constants.Constants;
import project.services.CreateService;
import project.services.DeleteService;
import project.services.ReadService;
import project.services.UpdateService;
import sample.controllers.AccountUserGameCrudController;
import sample.controllers.GenericController;
import sample.controllers.HomeController;
import sample.controllers.UserGameCrudController;
import sample.entity.Game;
import sample.entity.User;
import sample.entity.UserGame;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class UserGameCrudControllerImp extends GenericControllerImp implements
		UserGameCrudController {

	private HomeController homeController;
	private GenericController genericController;

	private User user;
	private UserGame userGame;

	// Services
	private CreateService createService;
	private ReadService readService;
	private UpdateService updateService;
	private DeleteService deleteService;

	private Map<Integer, UserGame> mpUserGames;
	private List<UserGame> lsDeleteUserGame;
	private List<UserGame> lsCreateUserGame;
	private List<UserGame> lsUpdateUserGame;

	@Override
	@PostConstruct
	public void init() {
		createService = super.findBean(CreateService.class);
		readService = super.findBean(ReadService.class);
		updateService = super.findBean(UpdateService.class);
		deleteService = super.findBean(DeleteService.class);
		
		homeController = super.findBean(HomeController.class);
		genericController = super.findBean(GenericController.class);

		this.user = homeController.getUser();
		this.fillGames();
	}

	@Override
	public String actionSaveUserGame() {
		try {
			lsDeleteUserGame = new ArrayList<UserGame>();
			lsCreateUserGame = new ArrayList<UserGame>();
			lsUpdateUserGame = new ArrayList<UserGame>();

			for (UserGame userGame : this.mpUserGames.values()) {
				if (!userGame.getSelected()
						&& userGame.getIdUserGame() != Constants.ZERO_INT) {
					lsDeleteUserGame.add(userGame);
				} else if (userGame.getSelected()
						&& userGame.getIdUserGame() != Constants.ZERO_INT) {
					lsUpdateUserGame.add(userGame);
				} else if (userGame.getSelected()) {
					lsCreateUserGame.add(userGame);
				} else {
					user.removeUserGame(userGame);
				}
			}

			for (UserGame userGame : lsDeleteUserGame) {
				deleteService.deleteUserGame(userGame);
			}
			for (UserGame userGame : lsCreateUserGame) {
				createService.createUserGame(userGame);
			}
			for (UserGame userGame : lsUpdateUserGame) {
				updateService.updateUserGame(userGame);
			}

			return homeController.getBeanName();
		} catch (Exception e) {
			this.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectHome() {
		return homeController.getBeanName();
	}

	@Override
	public String redirectAccountUserGameCrud() {
		genericController.setEntity(this.userGame);
		return this.userGame.getGame().getAcronym()
				+ getBeanName(AccountUserGameCrudController.class);
	}

	private void fillGames() {
		// This user Games
		List<UserGame> lsUserGames = user.getUserGames();
		mpUserGames = new LinkedHashMap<Integer, UserGame>();
		for (UserGame userGame : lsUserGames) {
			userGame.setSelected(true);
			mpUserGames.put(userGame.getGame().getIdGame(), userGame);
		}

		// All Games
		List<Game> lsGames = readService.readAllGames();
		for (Game game : lsGames) {
			if (!mpUserGames.containsKey(game.getIdGame())) {
				UserGame userGame = new UserGame();
				userGame.setGame(game);
				userGame.setUser(this.user);
				userGame.setDtSince(new Date());
				mpUserGames.put(userGame.getGame().getIdGame(), userGame);
			}
		}
	}

	// Getters and Setters
	@Override
	public List<UserGame> getSelectUserGames() {
		List<UserGame> lsUserGames = new ArrayList<UserGame>();
		lsUserGames.addAll(this.mpUserGames.values());
		return lsUserGames;
	}

	@Override
	public void setSelectUserGames(List<UserGame> lsUserGames) {
		this.mpUserGames = new LinkedHashMap<Integer, UserGame>();
		for (UserGame userGame : lsUserGames) {
			this.mpUserGames.put(userGame.getIdUserGame(), userGame);
		}
	}

	public UserGame getUserGame() {
		return userGame;
	}

	public void setUserGame(UserGame userGame) {
		this.userGame = userGame;
	}

}
