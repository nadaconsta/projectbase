package sample.controllers.imp;

import project.services.CreateService;
import project.services.ReadService;
import sample.controllers.GroupPatternCrudController;
import sample.controllers.HomeController;
import sample.controllers.ScheduleCrudController;
import sample.controllers.ServerCrudController;
import sample.controllers.UserGameCrudController;
import sample.entity.User;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean(eager = true)
public class HomeControllerImp extends GenericControllerImp implements
		HomeController {
	User user;

	CreateService createService;
	ReadService readService;

	@Override
	@PostConstruct
	public void init() {
		user = new User();
		createService = super.findBean(CreateService.class);
		readService = super.findBean(ReadService.class);
	}

	@Override
	public String actionLogout() {
		this.init();
		return getBeanName(IndexControllerImp.class);
	}

	@Override
	public String redirectUserGameCrud() {
		return getBeanName(UserGameCrudController.class);
	}
	
	@Override
	public String redirectScheduleCrud() {
		return getBeanName(ScheduleCrudController.class);
	}

	@Override
	public String redirectServerCrud() {
		return getBeanName(ServerCrudController.class);
	}

	@Override
	public String redirectGroupPatternCrud() {
		return getBeanName(GroupPatternCrudController.class);
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}
}
