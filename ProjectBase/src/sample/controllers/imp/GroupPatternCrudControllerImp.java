package sample.controllers.imp;

import project.services.CreateService;
import sample.controllers.GroupPatternCrudController;
import sample.entity.GroupPattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(eager = true)
public class GroupPatternCrudControllerImp extends GenericControllerImp implements
GroupPatternCrudController {

	// Entities
	private GroupPattern groupPattern;

	// Services
	private CreateService createService;

	@Override
	@PostConstruct
	public void init() {
		this.groupPattern = new GroupPattern();
		this.createService = super.findBean(CreateService.class);
	}

	@Override
	public String actionSaveGroupPattern() {
		try {	
			this.groupPattern = createService.createGroupPattern(this.groupPattern);
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String redirectIndex() {
		return getBeanName(IndexControllerImp.class);
	}

	// Getters and Setters
	@Override
	public GroupPattern getGroupPattern() {
		return this.groupPattern;
	}

	@Override
	public void setGroupPattern(GroupPattern groupPattern) {
		this.groupPattern = groupPattern;
	}

}