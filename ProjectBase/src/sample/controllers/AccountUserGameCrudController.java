package sample.controllers;

import java.util.List;

import sample.entity.AccountUserGame;
import sample.entity.Server;

public interface AccountUserGameCrudController extends GenericController {

	String actionSaveAccountUserGame();

	String redirectHome();

	AccountUserGame getAccountUserGame();
	
	List<Server> getAccountUserGameServer();

	void setAccountUserGame(AccountUserGame accountUserGame);

}
