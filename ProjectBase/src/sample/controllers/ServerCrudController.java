package sample.controllers;

import sample.entity.Server;

public interface ServerCrudController extends GenericController {
	
	String actionSaveServer();

	String redirectIndex();

	Server getServer();

	void setServer(Server server);

}
