package sample.controllers;

import sample.entity.GroupPattern;

public interface GroupPatternCrudController extends GenericController {
	
	String actionSaveGroupPattern();

	String redirectIndex();

	GroupPattern getGroupPattern();

	void setGroupPattern(GroupPattern groupPattern);

}