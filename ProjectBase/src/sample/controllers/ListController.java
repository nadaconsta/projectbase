package sample.controllers;

import java.util.List;

import sample.entity.Day;
import sample.entity.Game;
import sample.entity.Proficiency;


public interface ListController extends GenericController {

	List<Proficiency> getUserProficiency();

	List<Proficiency> getUserGameProficiency();

	List<Game> getAllGames();

	List<Day> getScheduleTimeDay();


}
