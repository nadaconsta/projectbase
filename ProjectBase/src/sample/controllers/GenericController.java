package sample.controllers;

import project.managed.bean.GenericManagedBean;
import sample.entity.GenericEntity;

public interface GenericController extends GenericManagedBean {

	public void init();

	// This method should treat all gg specific exceptions to a proper message.
	public void sendMessage(Exception e);

	void setEntity(GenericEntity<?> genericEntity);

	GenericEntity<?> getEntity();

}
