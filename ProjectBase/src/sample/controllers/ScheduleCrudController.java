package sample.controllers;

import java.util.List;

import sample.entity.Schedule;
import sample.entity.ScheduleTime;

public interface ScheduleCrudController extends GenericController {

	String actionSaveSchedule();

	String actionAddScheduleTime();

	String actionDelScheduleTime();

	String redirectHome();

	List<Schedule> getSelectSchedules();

	void setSelectSchedules(List<Schedule> lsSelectSchedule);

	Schedule getSchedule();

	void setSchedule(Schedule schedule);

	ScheduleTime getScheduleTime();

	void setScheduleTime(ScheduleTime scheduleTime);

}
