package sample.controllers;

import java.util.List;

import sample.entity.UserGame;

public interface UserGameCrudController extends GenericController {

	String actionSaveUserGame();

	String redirectHome();

	String redirectAccountUserGameCrud();

	List<UserGame> getSelectUserGames();

	void setSelectUserGames(List<UserGame> lsSelectUserGames);

}
