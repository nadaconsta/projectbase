package sample.controllers;

import sample.entity.User;

public interface UserRegisterController extends GenericController {

	String actionSaveUser();

	String redirectIndex();

	User getUser();

	void setUser(User user);

}
