package sample.dao;

import sample.entity.Schedule;

public interface ScheduleDao extends GenericDao {
	Boolean deleteSchedule(Schedule schedule);

	Schedule createSchedule(Schedule schedule);

	Schedule updateSchedule(Schedule schedule);

}
