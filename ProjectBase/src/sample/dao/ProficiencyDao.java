package sample.dao;

import java.util.List;

import sample.entity.Proficiency;

public interface ProficiencyDao extends GenericDao{
	List<Proficiency> findProficiencyByType(String type);
}
