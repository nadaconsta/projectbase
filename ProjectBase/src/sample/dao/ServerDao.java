package sample.dao;

import sample.entity.Server;

public interface ServerDao extends GenericDao {
	Boolean deleteServer(Server server);

	Server createServer(Server server);

	Server updateServer(Server server);

}
