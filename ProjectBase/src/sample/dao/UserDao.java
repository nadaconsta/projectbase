package sample.dao;

import javax.persistence.NoResultException;

import sample.entity.User;

public interface UserDao extends GenericDao {
	User createUser(User user);

	User updateUser(User user);

	User findUserByEmailPassword(User user) throws NoResultException;

}
