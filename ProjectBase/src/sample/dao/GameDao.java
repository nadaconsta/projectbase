package sample.dao;

import java.util.List;

import sample.entity.Game;

public interface GameDao extends GenericDao{
	public List<Game> findAllGames();
}
