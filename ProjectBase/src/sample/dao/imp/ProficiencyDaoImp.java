package sample.dao.imp;

import sample.dao.ProficiencyDao;
import sample.entity.Proficiency;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

@ApplicationScoped
@ManagedBean(eager = true)
public class ProficiencyDaoImp extends GenericDaoImp implements ProficiencyDao {

	@Override
	public List<Proficiency> findProficiencyByType(String type) {
		TypedQuery<Proficiency> query = createQuery(
				"Proficiency.findByType", Proficiency.class);
		query.setParameter("type", type);
		return query.getResultList();
	}

}
