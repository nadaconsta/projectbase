package sample.dao.imp;

import sample.dao.GameDao;
import sample.entity.Game;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

@ApplicationScoped
@ManagedBean(eager = true)
public class GameDaoImp extends GenericDaoImp implements GameDao {

	@Override
	public List<Game> findAllGames() {
		TypedQuery<Game> query = createQuery("Game.findAll", Game.class);
		return query.getResultList();
	}

}
