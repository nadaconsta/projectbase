package sample.dao.imp;

import sample.dao.ServerDao;
import sample.entity.Server;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class ServerDaoImp extends GenericDaoImp implements ServerDao {

	@Override
	public Boolean deleteServer(Server server) {
		return delete(server);
	}

	@Override
	public Server createServer(Server server) {
		return (Server) saveOrUpdate(server);
	}

	@Override
	public Server updateServer(Server server) {
		return (Server) saveOrUpdate(server);
	}

}