package sample.dao.imp;

import sample.dao.UserDao;
import sample.entity.User;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

@ApplicationScoped
@ManagedBean(eager = true)
public class UserDaoImp extends GenericDaoImp implements UserDao {

	@Override
	public User createUser(User user) {
		return (User) saveOrUpdate(user);
	}

	@Override
	public User updateUser(User user) {
		return (User) saveOrUpdate(user);
	}

	@Override
	public User findUserByEmailPassword(User user) {
		TypedQuery<User> query = createQuery("User.findByEmailPassword",
				User.class);
		query.setParameter("email", user.getEmail());
		query.setParameter("password", user.getPassword());
		return query.getSingleResult();
	}

}
