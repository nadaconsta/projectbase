package sample.dao.imp;

import project.constants.ConfigConstants;
import project.managed.bean.imp.GenericManagedBeanImp;
import sample.dao.EntityManagerDao;
import sample.entity.GenericEntity;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@SessionScoped
@ManagedBean
public class EntityManagerDaoImp extends GenericManagedBeanImp implements
		EntityManagerDao {
	private EntityManager em;
	private EntityManagerFactory emf;

	@Override
	@PostConstruct
	public void init() {
		emf = Persistence
				.createEntityManagerFactory(ConfigConstants.PERSISTANCE_UNIT);
		em = emf.createEntityManager();
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}

		if (em.contains(entity)) {
			em.persist(entity);
		} else {
			entity = em.merge(entity);
		}

		em.flush();
		em.getTransaction().commit();
		em.refresh(entity);
		return entity;
	}

	@Override
	@Transactional
	public Boolean delete(GenericEntity<?> entity) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}

		if (!em.contains(entity)) {
			entity = findById(entity.getId(), entity.getClass());
		}
		em.remove(entity);

		em.flush();
		em.getTransaction().commit();
		return true;
	}

	@Override
	@Transactional
	public <T> TypedQuery<T> createQuery(String query, Class<T> class_) {
		return em.createNamedQuery(query, class_);
	}

	@Override
	@Transactional
	public void rollback() {
		em.getTransaction().rollback();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public <T> GenericEntity<T> findById(Integer id, Class<T> class_) {
		GenericEntity<T> entity = (GenericEntity<T>) em.find(class_, id);
		em.refresh(entity);
		return entity;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public <T> GenericEntity<T> refresh(GenericEntity<T> entity) {
		entity = (GenericEntity<T>) em.find(entity.getClass(), entity.getId());
		em.refresh(entity);
		return entity;
	}
}
