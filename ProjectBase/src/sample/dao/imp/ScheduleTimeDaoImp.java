package sample.dao.imp;

import sample.dao.ScheduleTimeDao;
import sample.entity.ScheduleTime;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class ScheduleTimeDaoImp extends GenericDaoImp implements
		ScheduleTimeDao {

	@Override
	public Boolean deleteScheduleTime(ScheduleTime scheduleTime) {
		return delete(scheduleTime);
	}

	@Override
	public ScheduleTime createScheduleTime(ScheduleTime scheduleTime) {
		return (ScheduleTime) saveOrUpdate(scheduleTime);
	}

	@Override
	public ScheduleTime updateScheduleTime(ScheduleTime scheduleTime) {
		return (ScheduleTime) saveOrUpdate(scheduleTime);
	}

}