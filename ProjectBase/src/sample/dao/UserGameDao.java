package sample.dao;

import sample.entity.UserGame;

public interface UserGameDao extends GenericDao {

	Boolean deleteUserGame(UserGame userGame);

	UserGame createUserGame(UserGame userGame);

	UserGame updateUserGame(UserGame userGame);

}
