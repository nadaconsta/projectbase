package sample.dao;

import java.util.List;

import sample.entity.Day;

public interface DayDao extends GenericDao{
	public List<Day> findAllDays();
}
