package sample.dao;

import project.managed.bean.GenericManagedBean;

public interface EntityManagerDao extends GenericManagedBean, GenericDao {
	void init();

	void rollback();
}
