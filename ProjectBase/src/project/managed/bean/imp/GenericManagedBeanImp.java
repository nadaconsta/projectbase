package project.managed.bean.imp;

import java.util.ResourceBundle;

import project.constants.ConfigConstants;
import project.managed.bean.GenericManagedBean;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 * The entity this bean refers to should never be retrieved directly, also
 * formatters and data conversions should be applied in this level.
 * 
 * @author Willian Bispo
 */

@ApplicationScoped
@ManagedBean(eager = true)
public class GenericManagedBeanImp implements GenericManagedBean {

	@Override
	public ResourceBundle getMsgsBundle() {
		return FacesContext
				.getCurrentInstance()
				.getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(),
						ConfigConstants.MSGS_PROPERTIES);
	}

	@Override
	public String getBeanName() {
		return getBeanName(this.getClass());
	}

	@Override
	public String getBeanName(Class<?> class_) {
		String name = class_.getSimpleName();
		name = name.substring(0, 1).toLowerCase()
				+ name.substring(1, name.length());
		if (!name.endsWith(ConfigConstants.BEAN_IMPLEMENTATION_SUFIX)) {
			name += ConfigConstants.BEAN_IMPLEMENTATION_SUFIX;
		}
		return name;
	}

	@Override
	public <T> T findBean(Class<T> class_) {
		FacesContext context = FacesContext.getCurrentInstance();
		String beanName = this.getBeanName(class_);
		return (T) context.getApplication().evaluateExpressionGet(context,
				"#{" + beanName + "}", class_);
	}

}
