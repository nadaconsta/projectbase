package project.managed.bean;

import java.util.ResourceBundle;


/**
 * The entity this bean refers to should never be retrieved directly, also
 * formatters and data conversions should be applied in this level.
 * 
 * @author Willian Bispo
 */

public interface GenericManagedBean {

	public String getBeanName(Class<?> class_);
	public String getBeanName();

	public <T> T findBean(Class<T> class_);
	public ResourceBundle getMsgsBundle();
}
