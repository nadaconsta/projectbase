package project.constants;

public final class ConfigConstants {
	// Hibernate persistance unit name
	public static final String PERSISTANCE_UNIT = "foodmiles";

	// Properties files base name
	public static final String MSGS_PROPERTIES = "msgs";

	//Sufix for all beans implementations
	public static final String BEAN_IMPLEMENTATION_SUFIX = "Imp";
}
