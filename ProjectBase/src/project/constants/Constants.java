package project.constants;

public final class Constants {
	// Strings
	public static final String EMPTY_STRING = "";
	
	public static final String USERGAME_PROFICIENCY_TYPE = "UG";
	public static final String USER_PROFICIENCY_TYPE = "U";
	
	public static final String PROFICIENCY_DESC = "proficiency_desc";
	public static final String SERVER_DESC = "server_desc";
	public static final String DAY_DESC = "day_desc";
	

	// Integers
	public static final int ZERO_INT = 0;

	// Standard page actions
	public static final String pgBack = "back";
	public static final String DEFAULT_SEPARATOR = "_";
}
