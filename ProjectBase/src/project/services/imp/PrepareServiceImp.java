package project.services.imp;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import project.services.PrepareService;
import sample.dao.UserDao;

@ApplicationScoped
@ManagedBean(eager = true)
public class PrepareServiceImp extends GenericServiceImp implements PrepareService {
	UserDao userDao;
	
	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
	}
}
