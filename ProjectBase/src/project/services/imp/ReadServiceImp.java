package project.services.imp;

import project.services.ReadService;
import sample.dao.DayDao;
import sample.dao.GameDao;
import sample.dao.GenericDao;
import sample.dao.ProficiencyDao;
import sample.dao.UserDao;
import sample.entity.Day;
import sample.entity.Game;
import sample.entity.GenericEntity;
import sample.entity.Proficiency;
import sample.entity.User;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class ReadServiceImp extends GenericServiceImp implements ReadService {
	GenericDao genericDao;
	UserDao userDao;
	GameDao gameDao;
	ProficiencyDao proficiencyDao;
	DayDao dayDao;

	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
		gameDao = super.findBean(GameDao.class);
		proficiencyDao = super.findBean(ProficiencyDao.class);
		dayDao = super.findBean(DayDao.class);
	}

	@Override
	public User readUserByEmailPassword(User user) {
		return userDao.findUserByEmailPassword(user);
	}

	@Override
	public <T> GenericEntity<T> readById(Integer id, Class<T> class_) {
		genericDao = super.findBean(GenericDao.class);
		return genericDao.findById(id, class_);
	}

	@Override
	public List<Game> readAllGames() {
		return gameDao.findAllGames();
	}

	@Override
	public List<Day> readAllDays() {
		return dayDao.findAllDays();
	}

	@Override
	public List<Proficiency> readProficiencyByType(String type) {
		return proficiencyDao.findProficiencyByType(type);
	}

}
