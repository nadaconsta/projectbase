package project.services.imp;

import project.services.CreateService;
import sample.dao.GroupPatternDao;
import sample.dao.ScheduleDao;
import sample.dao.ScheduleTimeDao;
import sample.dao.ServerDao;
import sample.dao.UserDao;
import sample.dao.UserGameDao;
import sample.entity.GroupPattern;
import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.Server;
import sample.entity.User;
import sample.entity.UserGame;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class CreateServiceImp extends GenericServiceImp implements
		CreateService {
	UserDao userDao;
	UserGameDao userGameDao;
	ScheduleDao scheduleDao;
	ScheduleTimeDao scheduleTimeDao;
	ServerDao serverDao;
	GroupPatternDao groupPatternDao;
	
	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
		userGameDao = super.findBean(UserGameDao.class);
		scheduleDao = super.findBean(ScheduleDao.class);
		scheduleTimeDao = super.findBean(ScheduleTimeDao.class);
		serverDao = super.findBean(ServerDao.class);
		groupPatternDao = super.findBean(GroupPatternDao.class);
	}

	@Override
	public User createUser(User user) {
		Schedule schedule = new Schedule();
		schedule = this.createSchedule(schedule);

		user.setSchedule(schedule);
		user = userDao.createUser(user);
		
		schedule.setUser(user);
		return user;
	}

	@Override
	public UserGame createUserGame(UserGame userGame) {
		User user = userGame.getUser();

		Schedule schedule = new Schedule();
		schedule = this.createSchedule(schedule);
		
		userGame.setSchedule(schedule);
		userGame = userGameDao.createUserGame(userGame);
		
		schedule.setUserGame(userGame);
		user.addUserGame(userGame);
		return userGame;
	}

	@Override
	public Schedule createSchedule(Schedule schedule) {
		User user = schedule.getUser();
		UserGame userGame = schedule.getUserGame();
		
		schedule = scheduleDao.createSchedule(schedule);
		
		if (user != null) {
			schedule.setUser(user);
			user.setSchedule(schedule);
		} else if (userGame != null) {
			schedule.setUserGame(userGame);
			userGame.setSchedule(schedule);
		}
		
		return schedule;
	}

	@Override
	public ScheduleTime createScheduleTime(ScheduleTime scheduleTime) {
		Schedule schedule = scheduleTime.getSchedule();
		schedule.removeScheduleTime(scheduleTime);
		
		scheduleTime.setSchedule(schedule);
		scheduleTime = scheduleTimeDao.createScheduleTime(scheduleTime);
		
		schedule.addScheduleTime(scheduleTime);
		
		return scheduleTime;
	}

	@Override
	public Server createServer(Server server) {
		server = serverDao.createServer(server);
		return server;
	}

	@Override
	public GroupPattern createGroupPattern(GroupPattern groupPattern) {
		groupPattern = groupPatternDao.createGroupPattern(groupPattern);
		return groupPattern;
	}
}
