package project.services.imp;

import project.services.DeleteService;
import project.services.ReadService;
import sample.dao.ScheduleDao;
import sample.dao.ScheduleTimeDao;
import sample.dao.UserDao;
import sample.dao.UserGameDao;
import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.User;
import sample.entity.UserGame;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(eager = true)
public class DeleteServiceImp extends GenericServiceImp implements
		DeleteService {
	UserDao userDao;
	UserGameDao userGameDao;
	ScheduleDao scheduleDao;
	ScheduleTimeDao scheduleTimeDao;

	ReadService readService;

	@Override
	@PostConstruct
	public void init() {
		userDao = super.findBean(UserDao.class);
		userGameDao = super.findBean(UserGameDao.class);
		scheduleTimeDao = super.findBean(ScheduleTimeDao.class);
		scheduleDao = super.findBean(ScheduleDao.class);
		readService = super.findBean(ReadService.class);
	}

	@Override
	public void deleteUserGame(UserGame userGame) {
		User user = userGame.getUser();
		Schedule schedule = userGame.getSchedule();
		userGameDao.deleteUserGame(userGame);
		this.deleteSchedule(schedule);
		user.removeUserGame(userGame);
	}

	@Override
	public void deleteScheduleTime(ScheduleTime scheduleTime) {
		Schedule schedule = scheduleTime.getSchedule();
		schedule.addScheduleTime(scheduleTime);
		scheduleTimeDao.deleteScheduleTime(scheduleTime);
		schedule.removeScheduleTime(scheduleTime);
	}

	@Override
	public void deleteSchedule(Schedule schedule) {
		User user = schedule.getUser();
		UserGame userGame = schedule.getUserGame();
		List<ScheduleTime> lsScheduleTime = schedule.getScheduleTimes();
		for(ScheduleTime scheduleTime : lsScheduleTime){
			this.deleteScheduleTime(scheduleTime);
		}
		scheduleDao.deleteSchedule(schedule);
		if (user != null) {
			user.setSchedule(null);
		} else if (userGame != null) {
			userGame.setSchedule(null);
		}
	}
}
