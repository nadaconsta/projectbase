package project.services;

import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.UserGame;

public interface DeleteService extends GenericService {

	void deleteUserGame(UserGame userGame);

	void deleteScheduleTime(ScheduleTime scheduleTime);

	void deleteSchedule(Schedule schedule);

}
