package project.services;

import project.managed.bean.GenericManagedBean;


// This is the interface common to all services
public interface GenericService extends GenericManagedBean {
	void init();
}
