package project.services;

import java.util.List;

import sample.entity.Day;
import sample.entity.Game;
import sample.entity.GenericEntity;
import sample.entity.Proficiency;
import sample.entity.User;

public interface ReadService extends GenericService {
	<T> GenericEntity<T> readById(Integer id, Class<T> class_);

	// Get the user using specific parameter of a non persisted entity
	User readUserByEmailPassword(User user);

	List<Game> readAllGames();

	List<Proficiency> readProficiencyByType(String type);

	List<Day> readAllDays();

}
