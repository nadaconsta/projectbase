package project.services;

import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.User;
import sample.entity.UserGame;

public interface UpdateService extends GenericService {
	User updateUser(User user);

	UserGame updateUserGame(UserGame userGame);

	Schedule updateSchedule(Schedule schedule);

	ScheduleTime updateScheduleTime(ScheduleTime scheduleTime);
}
