package project.services;

import sample.entity.GroupPattern;
import sample.entity.Schedule;
import sample.entity.ScheduleTime;
import sample.entity.Server;
import sample.entity.User;
import sample.entity.UserGame;

public interface CreateService extends GenericService {
	User createUser(User user);

	UserGame createUserGame(UserGame userGame);

	Schedule createSchedule(Schedule schedule);

	ScheduleTime createScheduleTime(ScheduleTime scheduleTime);
	
	Server createServer(Server server);
	
	GroupPattern createGroupPattern(GroupPattern groupPattern);
}
