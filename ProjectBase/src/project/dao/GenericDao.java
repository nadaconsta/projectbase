package project.dao;

import project.managed.bean.GenericManagedBean;
import sample.entity.GenericEntity;

import javax.persistence.TypedQuery;

public interface GenericDao extends GenericManagedBean {
	<T> GenericEntity<T> findById(Integer id, Class<T> class_);

	Boolean delete(GenericEntity<?> entity);

	<T> TypedQuery<T> createQuery(String query, Class<T> class_);

	<T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity);

	<T> GenericEntity<T> refresh(GenericEntity<T> entity);
}
