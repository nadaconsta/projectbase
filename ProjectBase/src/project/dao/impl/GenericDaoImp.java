package project.dao.impl;

import project.managed.bean.imp.GenericManagedBeanImp;
import sample.dao.EntityManagerDao;
import sample.dao.GenericDao;
import sample.entity.GenericEntity;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@ApplicationScoped
@ManagedBean(eager = true)
public class GenericDaoImp extends GenericManagedBeanImp implements GenericDao {
	private EntityManagerDao entityManagerDao;

	@Override
	@Transactional
	public <T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity) {
		entityManagerDao = findBean(EntityManagerDao.class);
		try {
			return entityManagerDao.saveOrUpdate(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public Boolean delete(GenericEntity<?> entity) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.delete(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> TypedQuery<T> createQuery(String query, Class<T> class_) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.createQuery(query, class_);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> findById(Integer id, Class<T> class_) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.findById(id, class_);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> refresh(GenericEntity<T> entity) {
		try {
			return entityManagerDao.refresh(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

}
