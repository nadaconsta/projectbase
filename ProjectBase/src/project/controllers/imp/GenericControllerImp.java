package project.controllers.imp;

import project.exceptions.ErrorException;
import project.exceptions.InfoException;
import project.exceptions.WarnException;
import project.managed.bean.imp.GenericManagedBeanImp;
import sample.controllers.GenericController;
import sample.entity.GenericEntity;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@SessionScoped
@ManagedBean(eager = true)
public class GenericControllerImp extends GenericManagedBeanImp implements
		GenericController {
	GenericEntity<?> entity;

	@Override
	public void init() {
		throw new NotImplementedException();
	}

	@Override
	public void sendMessage(Exception e) {
		if (InfoException.class.equals(e.getClass())) {
			InfoException me = (InfoException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, me.getCode()
							.toString(), me.getMessage()));
		} else if (WarnException.class.equals(e.getClass())) {
			WarnException me = (WarnException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, me.getCode()
							.toString(), me.getMessage()));
		} else if (ErrorException.class.equals(e.getClass())) {
			ErrorException ee = (ErrorException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ee.getCode()
							.toString(), ee.getMessage()));
		} else {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "999999",
							"Fatal Error."));
		}
	}
	
	@Override
	public GenericEntity<?> getEntity(){
		return this.entity;
	}
	
	@Override
	public void setEntity(GenericEntity<?> genericEntity){
		this.entity = genericEntity;
	}
}
